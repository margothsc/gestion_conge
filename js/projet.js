
$(document).ready(function() { // on page load
  $(".projet .modifier").click(function(){
      var line_id = $(this).attr('line-id');
      $("#line"+line_id+" .modifier").hide();
      $("#line"+line_id+" .supprimer").hide();
      $("#line"+line_id+" .valider").show();
      $("#line"+line_id+" .text_line").hide();
      $("#line"+line_id+" .input_line").show();
      return false;        
  });

  $(".projet .valider").click(function(){
    var line_id = $(this).attr('line-id');
    var nom = $("#line"+line_id+" .nom").val();
    var dataString = 'nom='+nom+'&id='+line_id;
      
	//requete ajax
    $.ajax({
      type: "POST",
      url: "index.php?action=modifier_projet",
      data: dataString,
      success: function(response){
	      fillHtmlElement(response, "page");
	      $.getScript('js/projet.js');
      },
      fail : function(){
        alert("error");
      }
    });
    return false;
  });
  
  $(".projet .supprimer").click(function(){
    var line_id = $(this).attr('line-id');
    if(confirm("êtes vous sur de vouloir supprimer ce projet ?")){
      $("#line"+line_id+" .text_line").hide();
      $("#line"+line_id+" .input_line").hide();
      var dataString = 'id='+line_id ;
      
	  //requete ajax
      $.ajax({
        type: "POST",
        url: "index.php?action=supprimer_projet",
        data: dataString,
        success: function(response){
	      fillHtmlElement(response, "page");
	      $.getScript('js/projet.js');
        },
        fail : function(){
          alert("error");
        }
      });
    }
    return false;
  });
        
	$('#newNom').keyup(function(e) {  
      if(e.keyCode == 13) { // KeyCode de la touche entrée
        nouveau_projet();
      }
    });
  
  $("#ajouter_projet").click(function(e){
	e.preventDefault();
    nouveau_projet();
  });

  function verif_champs(nom) {
	var bon = true;
	
	$(".erreur").remove();
	
	if(nom == "") {
	  $("#newNom").parent().append("<span class='erreur'> Ce champs est requis. </span> ");
	  bon = false;
	}
	return bon;
  }

  function nouveau_projet() {
    var nom = $("#newNom").val();
	var dataString = 'nom='+nom;
      
	if(verif_champs(nom)) {
	  //requete ajax
      $.ajax({
        type: "POST",
        url: "index.php?action=nouveau_projet",
        data: dataString,
        success: function(response){
	      fillHtmlElement(response, "page");
	      $.getScript('js/projet.js');
        },
        fail : function(){
          alert("error");
        }
      });
      return false;
	 }
  }
});
