
$(document).ready(function() { // on page load
  $(".modifier_salarie").click(function(){
      $(".modifier_salarie").hide();
      $(".supprimer_salarie").hide();
      $(".valider_salarie").show();
      $(".salarie .text_line").hide();
      $(".salarie .input_line").show();
      return false;        
  })

  $(".valider_salarie").click(function(){
    var line_id = $(this).attr('line-id');
    var code = $(".salarie .code").val();
    var nom = $(".salarie .nom").val();
    var prenom = $(".salarie .prenom").val();
    var mail = $(".salarie .mail").val();
    var dataString = 'code='+code+'&nom='+nom+'&prenom='+prenom+'&mail='+mail+'&id_salarie='+line_id;
    
	//requete ajax
    $.ajax({
      type: "POST",
      url: "index.php?action=modifier_fiche_salarie",
      data: dataString,
      success: function(response){
	    fillHtmlElement(response, "page");
	    $.getScript('js/salarie.js');
	    $.getScript('js/fiche_salarie.js');
      },
      fail : function(response){
        alert("error");
      }
    });
    return false;
  })
  
  $('.debut').datepicker({
    minDate : new Date(), 
	dateFormat : 'dd-mm-yy',
    onSelect: function () {
      if ($(this).val() != '') {
        $(".fin").datepicker('option', 'minDate', $(this).val());
      }
    }
  });
  
  $('.fin').datepicker({
	dateFormat : 'dd-mm-yy'
  });
  
  $("#nouveau_projet_salarie").click(function(e){
    e.preventDefault();
	
    var id = $("#newProjet .id-salarie").val();
    var projet = $("#newProjet .libelle option:selected").val();
    var debut = $("#newProjet .debut").val();
    var fin = $("#newProjet .fin").val();
    var dataString = 'projet='+projet+'&debut='+debut+'&fin='+fin+'&id='+id;
	
	//requete ajax
    $.ajax({
      type: "POST",
      url: "index.php?action=nouveau_projet_salarie",
      data: dataString,
      success: function(response){
	    fillHtmlElement(response, "fiche");
	    $.getScript('js/fiche_salarie.js');
      },
      fail : function(response){
        alert("error");
      }
    });
    return false;
  })

  $("#fiche .modifier").click(function(){
    var id_projet = $(this).parent().parent().attr('id-projet');
      $(".projet_salarie"+id_projet+" .modifier").hide();
      $(".projet_salarie"+id_projet+" .supprimer").hide();
      $(".projet_salarie"+id_projet+" .valider").show();
      $(".projet_salarie"+id_projet+" .text_line").hide();
      $(".projet_salarie"+id_projet+" .input_line").show();
      return false;        
  })
  
  $("#fiche .supprimer").click(function(){
    var id_salarie = $(this).parent().parent().attr('id-salarie');
    var id_projet = $(this).parent().parent().attr('id-projet');
    var dataString = 'id_salarie='+id_salarie+'&id_projet='+id_projet ;
    
	//requete ajax
    $.ajax({
      type: "POST",
      url: "index.php?action=supprimer_projet_salarie",
      data: dataString,
	  dataType : 'html',
      success: function(response){
	    fillHtmlElement(response, "fiche");
	    $.getScript('js/fiche_salarie.js');
      },
      fail : function(){
        alert("error");
      }
    });
    return false;
  })
  
  $("#fiche .valider").click(function(){
    var id_salarie = $(this).parent().parent().attr('id-salarie');
    var id_projet = $(this).parent().parent().attr('id-projet');
    var debut = $(".projet_salarie"+id_projet+" .debut").val();
    var fin = $(".projet_salarie"+id_projet+" .fin").val();
    var dataString = 'id_salarie='+id_salarie+'&id_projet='+id_projet+'&debut='+debut+'&fin='+fin;
    
	//requete ajax
    $.ajax({
      type: "POST",
      url: "index.php?action=modifier_projet_salarie",
      data: dataString,
      success: function(response){
	    fillHtmlElement(response, "fiche");
	    $.getScript('js/fiche_salarie.js');
      },
      fail : function(response){
        alert("error");
      }
    });
    return false;
  })
});
