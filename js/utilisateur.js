
$(document).ready(function() { // on page load
  $(".utilisateur .modifier").click(function(){
      var line_id = $(this).attr('line-id');
      $("#line"+line_id+" .modifier").hide();
      $("#line"+line_id+" .supprimer").hide();
      $("#line"+line_id+" .valider").show();
      $("#line"+line_id+" .text_line").hide();
      $("#line"+line_id+" .input_line").show();
      return false;        
  });

  $(".utilisateur .valider").click(function(){
    var line_id = $(this).attr('line-id');
    var login = $("#line"+line_id+" .log").val();
    var mail = $("#line"+line_id+" .mail").val();
    var droit = $("#line"+line_id+" .role option:selected").val();
    var dataString = 'login='+login+'&mail='+mail+'&droit='+droit+'&id='+line_id;
      
	//requete ajax
    $.ajax({
      type: "POST",
      url: "index.php?action=modifier_utilisateur",
      data: dataString,
      success: function(response){
	      fillHtmlElement(response, "page");
	      $.getScript('js/utilisateur.js');
      },
      fail : function(response){
        alert("error");
      }
    });
    return false;
  });
  
  $(".utilisateur .supprimer").click(function(){
    var line_id = $(this).attr('line-id');
    if(confirm("êtes vous sur de vouloir supprimer cet utilisateur ?")){
      $("#line"+line_id+" .text_line").hide();
      $("#line"+line_id+" .input_line").hide();
      var line_id = $(this).attr('line-id');
      var dataString = 'id='+line_id ;
      
	  //requete ajax
      $.ajax({
        type: "POST",
        url: "index.php?action=supprimer_utilisateur",
        data: dataString,
        success: function(){
	      fillHtmlElement(response, "page");
	      $.getScript('js/utilisateur.js');
        },
        fail : function(){
          alert("error");
        }
      });
    }
    return false;
  });
        
	$('#newLog').keyup(function(e) {  
      if(e.keyCode == 13) { // KeyCode de la touche entrée
        nouvel_utilisateur();
      }
    });
        
	$('#newMail').keyup(function(e) {  
      if(e.keyCode == 13) { // KeyCode de la touche entrée
        nouvel_utilisateur();
      }
    });
  
  $("#ajouter_utilisateur").click(function(e){
	e.preventDefault();
    nouvel_utilisateur();
  });

  function verif_champs(login, mail) {
	var bon = true;
	// regex pour la validité du mail
	var reg = new RegExp('^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$', 'i');
	
	$(".erreur").remove();
	
	if(login == "") {
	  $("#newLog").parent().append("<span class='erreur'> Ce champs est requis. </span> ");
	  bon = false;
	}
	if(mail == "") {
	  $("#newMail").parent().append("<span class='erreur'> Ce champs est requis. </span> ");
	  bon = false;
	}
	else if(!reg.test(mail))
	{
	  $("#newMail").parent().append("<span class='erreur'> L'adresse mail n'est pas valide. </span> ");
	  bon = false;
	}
	return bon;
  }

  function nouvel_utilisateur() {
  
    var login = $("#newLog").val();
    var mail = $("#newMail").val();
    var droit = $("#newRole").val();
	var mdp = SHA256(login);
	var dataString = 'login='+login+'&mail='+mail+'&droit='+droit+'&mdp='+mdp;
     
	if(verif_champs(login, mail)) {
      //requete ajax
      $.ajax({
        type: "POST",
        url: "index.php?action=nouvel_utilisateur",
        data: dataString,
        success: function(response){
	      fillHtmlElement(response, "page");
	      $.getScript('js/utilisateur.js');
        },
        fail : function(){
          alert("error");
        }
      });
	}
    return false;
  };
});
