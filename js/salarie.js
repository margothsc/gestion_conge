
$(document).ready(function() { // on page load
  $(".listeSalarie .supprimer, #ficheSalarie .supprimer").click(function(){
	  if(confirm("êtes vous sur de vouloir supprimer ce salarie ?")){
	    $("#line"+line_id+" .text_line").hide();
	    $("#line"+line_id+" .input_line").hide();
        var line_id = $(this).attr('line-id');
        var dataString = 'id='+line_id ;
      
	    //requete ajax
        $.ajax({
          type: "POST",
          url: "index.php?action=supprimer_salarie",
          data: dataString,
          success: function(response){
	        fillHtmlElement(response, "page");
	        $.getScript('js/salarie.js');
          },
          fail : function(){
            alert("error");
          }
        });
      }
   });
        
	$('#newCode').keyup(function(e) {  
      if(e.keyCode == 13) { // KeyCode de la touche entrée
        nouveau_salarie();
      }
    });
        
	$('#newNom').keyup(function(e) {  
      if(e.keyCode == 13) { // KeyCode de la touche entrée
        nouveau_salarie();
      }
    });
        
	$('#newPrenom').keyup(function(e) {  
      if(e.keyCode == 13) { // KeyCode de la touche entrée
        nouveau_salarie();
      }
    });
        
	$('#newMail').keyup(function(e) {  
      if(e.keyCode == 13) { // KeyCode de la touche entrée
        nouveau_salarie();
      }
    });
  
  $("#ajouter_salarie").click(function(e){
    e.preventDefault();
  
    nouveau_salarie();
  });

  function verif_champs(code, nom, prenom, mail) {
	var bon = true;
	// regex pour la validité du mail
	var reg = new RegExp('^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$', 'i');
	
	$(".erreur").remove();
	
	if(code == "") {
	  $("#newCode").parent().append("<span class='erreur'> Ce champs est requis. </span> ");
	  bon = false;
	} 
	if(nom == "") {
	  $("#newNom").parent().append("<span class='erreur'> Ce champs est requis. </span> ");
	  bon = false;
	}
	if(prenom == "") {
	  $("#newPrenom").parent().append("<span class='erreur'> Ce champs est requis. </span> ");
	  bon = false;
	}
	if(mail == "") {
	  $("#newMail").parent().append("<span class='erreur'> Ce champs est requis. </span>");
	  bon = false;
	}
	else if(!reg.test(mail))
	{
	  $("#newMail").parent().append("<span class='erreur'> L'adresse mail n'est pas valide. </span> ");
	  bon = false;
	}
	return bon;
  }
  
  function nouveau_salarie() {
    var code = $("#newCode").val();
    var nom = $("#newNom").val();
    var prenom = $("#newPrenom").val();
    var mail = $("#newMail").val();
	var dataString = 'code='+code+'&nom='+nom+'&prenom='+prenom+'&mail='+mail;
	
	if(verif_champs(code, nom, prenom, mail)) {
	  //requete ajax
      $.ajax({
        type: "POST",
        url: "index.php?action=nouveau_salarie",
        data: dataString,
	    success: function(response){
	      fillHtmlElement(response, "page");
	      $.getScript('js/salarie.js');
	    },
        fail : function(){
          alert("error");
        }
      });
	}
  }
});
