$(document).ready(function() { // on page load
	
  $("#mois").datepicker({     
    dateFormat: 'MM yy', 
	monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin', 'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],   
	monthNamesShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Juin", "Juil", "Aoû", "Sep", "Oct", "Nov", "Dec" ],
	changeMonth: true,
    changeYear: true,
	onClose: function (dateText, inst) {
	  var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	  var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	  $(this).datepicker('setDate', new Date(year, month));
	  changerMois();
	},
	minDate: "-1M", 
	maxDate: "+1M"
  });
	
  $("#mois").focus(function () {
    $(".ui-datepicker-calendar").hide();
  });
	
  function changerMois(){
    var id_modele = $("#saisie2").attr('id-modele');
    var mois = $('#mois').val();
	var dataString = 'id_modele='+id_modele+'&mois='+mois ;

	$.ajax({
	  type: "POST",
	  url: "index.php?action=changer_mois_saisie",
	  data: dataString,
	  datatType : 'json',
	  success: function(response){
	    fillHtmlElement(response, "modele");
	    $.getScript('js/modele.js');
	  },
	  fail : function(response){
	    alert("error");
	  }
	});
	return false;   
  }
  
  $(".choixModeleSaisie").click(function(){
    var id_modele = $(this).attr('id-modele');
	var dataString = 'id_modele='+id_modele ;
	
	$.ajax({
	  type: "POST",
	  url: "index.php?action=changer_modele_saisie",
	  data: dataString,
	  datatType : 'json',
	  success: function(response){
	    fillHtmlElement(response, "saisie");
	    $.getScript('js/modele.js');
	  },
	  fail : function(response){
	    alert("error");
	  }
	});
	return false;        
  });
	
  $('#saisie2 .valider').click(function() {
	var id_salarie_champs = $(this).parent().parent().attr('class');
	var nbr_jours = $('.'+id_salarie_champs+' .nbr_jours').val();
	var commentaire = $('.' +id_salarie_champs+ ' .commentaire').val();
	var modele = $("#saisie2").attr('id-modele');
	var dataString = 'nbr_jours='+nbr_jours+'&commentaire='+commentaire+'&id_salarie_champs='+id_salarie_champs+'&modele='+modele;
	
	//requete ajax
	$.ajax({
	  type: "POST",
	  url: "index.php?action=valider_modele_saisie",
	  data: dataString,
	  datatType : 'json',
	  success: function(response){
		fillHtmlElement(response, "saisie");
		$.getScript('js/modele.js');
	  },
	  fail : function(){
	    alert("error");
	  }
    });
  });
	
  $('#saisie2 .supprimer').click(function() {
    if(confirm("êtes vous sur de vouloir supprimer ce champs ?")){
	  var id_salarie_champs = $(this).parent().parent().attr('class');
	  var saisie = $("#saisie2").attr('id-saisie');
	  var modele = $("#saisie2").attr('id-modele');
	  var dataString = 'id_salarie_champs='+id_salarie_champs+'&saisie='+saisie+'&modele='+modele;
	
	  //requete ajax
	  $.ajax({
	    type: "POST",
	    url: "index.php?action=supprimer_modele_saisie",
	    data: dataString,
	    datatType : 'json',
	    success: function(response){
		fillHtmlElement(response, "saisie");
		$.getScript('js/modele.js');
	    },
	    fail : function(){
		  alert("error");
	    }
	  });
	}
  });
  
  $(".choixModele").click(function(){
    var id_modele = $(this).attr('id-modele');
	var dataString = 'id_modele='+id_modele ;

	//requete ajax
	$.ajax({
	  type: "POST",
	  url: "index.php?action=changer_modele_imputation",
	  data: dataString,
	  datatType : 'json',
	  success: function(response){
	    fillHtmlElement(response, "page");
	    $.getScript('js/modele.js');
	  },
	  fail : function(response){
	    alert("error");
	  }
	});
	return false;        
  });
	
  $("#nouveau_modele").submit(function(e){
	e.preventDefault();
	
	var nom = $('#nom').val();
	var dataString = 'nom='+nom;
		
	//requete ajax
	$.ajax({
	  type: "POST",
	  url: "index.php?action=nouveau_modele_imputation",
	  data: dataString,
	  datatType : 'json',
	  success: function(response){
        fillHtmlElement(response, "page");
	    $.getScript('js/modele.js');
	  },
	  fail : function(response){
	    alert("error");
	  }
	});
	return false;        
  });
	
  $(".lier").click(function(){
	var projet = $('#projet option:selected').val();
    var id_modele = $("#modele").attr('id-modele');
	var dataString = 'projet='+projet+'&modele='+id_modele ;
		
	//requete ajax
	$.ajax({
	  type: "POST",
	  url: "index.php?action=lier_modele_imputation",
	  data: dataString,
	  datatType : 'json',
	  success: function(response){
        fillHtmlElement(response, "liaison");
	  },
	  fail : function(response){
	    alert("error");
	  }
	});
	return false;        
  });
	
  $("#copier_modele").submit(function(e){
	e.preventDefault();
	
    var id_modele = $("#modele").attr('id-modele');
	var nom = $("#nomCopie").val();
	var dataString = 'modele='+id_modele+'&nom='+nom ;
	
	//requete ajax
	$.ajax({
	  type: "POST",
	  url: "index.php?action=copier_modele_imputation",
	  data: dataString,
	  datatType : 'json',
	  success: function(response){
        fillHtmlElement(response, "page");
	    $.getScript('js/modele.js');
	  },
	  fail : function(response){
	    alert("error");
	  }
	});
	return false;        
  });
	
  $("#modele .modifier").click(function(){
    var line_id = $(this).parent().parent().attr('id');
    $("#"+line_id+" .modifier").hide();
    $("#"+line_id+" .supprimer").hide();
    $("#"+line_id+" .valider").show();
    $("#"+line_id+" .text_line").hide();
    $("#"+line_id+" .input_line").show();
    $("#"+line_id+" .decompte").removeAttr('disabled');
    return false;        
  });

  $("#modele .valider").click(function(){
	  var id_modele = $("#modele").attr('id-modele');
      var id_modele_champs = $(this).attr('id');
      var type_activite = $(".type_activite").val();
      var type_champs = $(".type_champs").val();
      var decompte = $(".decompte").is(":checked");
      var aide = $(".aide").val();
      var dataString = 'type_activite='+type_activite+'&type_champs='+type_champs+'&decompte='+decompte+'&aide='+aide+'&id_modele_champs='+id_modele_champs+'&id_modele='+id_modele ;
     
	 //requete ajax
      $.ajax({
      type: "POST",
        url: "index.php?action=modifier_modele_champs",
        data: dataString,
		datatType : 'html',
          success: function(response){
            fillHtmlElement(response, "modele");
	        $.getScript('js/modele.js');
          },
          fail : function(response){
              alert("error");
          }
      });
      return false;
  });
  
  $("#modele .supprimer").click(function(){
    if(confirm("êtes vous sur de vouloir supprimer ce champs ?")){
      var id_modele_champs = $(this).attr('id');
	  var id_modele = $("#modele").attr('id-modele');
      var dataString = 'id_modele_champs='+id_modele_champs+'&id_modele='+id_modele ;
      
	  //requete ajax
      $.ajax({
        type: "POST",
        url: "index.php?action=supprimer_modele_champs",
        data: dataString,
        success: function(response){
          fillHtmlElement(response, "modele");
	      $.getScript('js/modele.js');
        },
        fail : function(){
          alert("error");
        }
      });
    }
    return false;
  });
  
  $("#nouveau_champs").click(function(e){
	  e.preventDefault();
  
      nouveau_champs();
  });

  function verif_champs(type_activite) {
	var bon = true;
	// regex pour la validité du mail
	var reg = new RegExp('^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$', 'i');
	
	$(".erreur").remove();
	
	if(type_activite == "") {
	  $("#type_activite").parent().append("<span class='erreur'> Ce champs est requis. </span> ");
	  bon = false;
	} 
	return bon;
  }
  
  function nouveau_champs() {
	
      var id_modele = $("#modele").attr('id-modele');
      var type_activite = $("#type_activite").val();
      var type_champs = $("#type_champs").val();
      var decompte = $("#decompte").is(":checked");
      var aide = $("#aide").val();
      var dataString = 'type_activite='+type_activite+'&type_champs='+type_champs+'&decompte='+decompte+'&aide='+aide+'&id_modele='+id_modele;
      
	if(verif_champs(type_activite)) {
	  //requete ajax
      $.ajax({
        type: "POST",
        url: "index.php?action=nouveau_modele_champs",
        data: dataString,
        success: function(response){
          fillHtmlElement(response, "modele");
	      $.getScript('js/modele.js');
        },
        fail : function(response){
          alert("error");
        }
      });
      return false;
	}
  };
  
  $("#decompte").mouseover(function() {
	  $(this).parent().append("<span class='info'> décompte des jours </span> ");
  });
  
  $("#decompte").mouseleave(function() {
	$(".info").remove();
  });
  
  $(".decompte").mouseover(function() {
	  $(this).parent().append("<span class='info'> décompte des jours </span> ");
  });
  
  $(".decompte").mouseleave(function() {
	$(".info").remove();
  });
});
