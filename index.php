<?php	
require_once('controller/Controller.class.php');

	// Création de l'architecture MVC
	$oCtrl = new Controller();
		
	// Authentification
	if($oCtrl->Authorize())
		$oCtrl->dispatcher();