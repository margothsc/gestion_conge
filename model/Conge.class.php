<?php
	require_once 'Model.class.php';
	
	class Conge extends Model{
	
		function getOneConge($semestre, $annee, $projet) {
			$sql = 'SELECT * 
			        FROM conge c, projet p
					WHERE semestre = ?
					AND annee = ?
					AND c.id_projet = ?
					AND c.id_projet = p.id_projet';
			
			$conge = $this->createRequest($sql, array($semestre, $annee, $projet));
	 
			if ($conge->rowCount() > 0)
				  return $conge->fetch(); 
			else
				return null;
		}
	
		function getRecentConge() {
			$sql = 'SELECT nom, libelle_projet, MAX(semestre) as semestre, MAX(annee) as annee
			        FROM conge c, projet p
					WHERE c.id_projet = p.id_projet';
			
			$conge = $this->createRequest($sql);
	 
			if ($conge->rowCount() > 0)
				  return $conge->fetch(); 
			else
				return null;
		}
		
		function addConge($semestre, $annee, $projet, $nom) {
			$sql = 'INSERT INTO conge (semestre, annee, id_projet, nom)
					VALUES (?, ?, ?, ?)';
					
			$this->createRequest($sql, array($semestre, $annee, $projet, $nom));
		}
		
		function updateConge($nom, $id) {
			$sql = 'UPDATE conge 
					SET nom = ?
					WHERE id_conge = ?';
					
			$this->createRequest($sql, array($nom, $id));
		}
	}