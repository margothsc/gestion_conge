<?php
	require_once 'Model.class.php';

	class Utilisateur extends Model{

		function isLoggedIn() {
			if(!isset($_SESSION))
				return FALSE;
			if(!isset($_SESSION['utilisateur']['login']))
				return FALSE;
			
			return TRUE;
		}
    
		function Encrypt($mdp,$graine){
			for($j=0;$j<$graine;$j++) {
				$mdp .= ".";
			}
			
			return hash("sha256",$mdp,false);
		}

		function hard_session_destroy() {
			// Détruit toutes les variables de session
			$_SESSION = array();
		}
		function getOneUtilisateur($login) {
			$sql='SELECT *
				 FROM utilisateur u, droit d
				  WHERE login = ?
				  AND u.id_droit = d.id_droit';
			
			$user = $this->createRequest($sql,array($login));
	 
			if ($user->rowCount() > 0)
				  return $user->fetch(); 
			else
				return null;
		}
		
		function getAllUtilisateur() {
			$sql='SELECT *
				 FROM utilisateur u, droit d
				  WHERE u.id_droit = d.id_droit';
			
			$user = $this->createRequest($sql);
	 
			if ($user->rowCount() > 0)
				  return $user->fetchAll(); 
			else
				return null;
		}
		
		function addUtilisateur($login, $mdp, $mail, $droit) {
			$date = date(DATE_W3C);
			
			$sql='INSERT INTO utilisateur(login, mot_de_passe, mail, id_droit, date_inscription)
				  VALUES(?, ?, ?, ?, ?)';
			
			$this->createRequest($sql, array($login, $mdp, $mail, 2, $date));
		}
		
		function setMdpUtilisateur($login, $mdp) {
			$sql='UPDATE utilisateur
				  SET mot_de_passe = ?
				  WHERE login = ?';
			
			$this->createRequest($sql, array($mdp, $login));
		}
		
		function updateUtilisateur($id, $login, $mail, $role) {
			$sql='UPDATE utilisateur
				  SET id_droit = ?,
				  login = ?,
				  mail = ?
				  WHERE id_utilisateur = ?';
			
			$this->createRequest($sql, array($role, $login, $mail, $id));
		}
		
		function deleteUtilisateur($id) {
			$sql='DELETE FROM utilisateur
				  WHERE id_utilisateur = ?';
			
			$this->createRequest($sql, array($id));
		}
		
		function isMdpUtilisateur($login, $mdp) {
			$sql='SELECT *
				  FROM utilisateur
				  WHERE login = ?
				  AND mot_de_passe = ?';
			
			$user = $this->createRequest($sql,array($login, $mdp));
	 
			if ($user->rowCount() > 0)
				  return TRUE;
			else
				return FALSE;
		}
		
		function getAllDroit() {
			$sql='SELECT *
				  FROM droit';
			
			$droits = $this->createRequest($sql);
	 
			if ($droits->rowCount() > 0)
				  return $droits->fetchAll(); 
			else
				return null;
		}
		
		function verifierAdresseMail($adresse) {
			//Caractères non-ASCII autorisés dans un nom de domaine .eu :
			$nonASCII='ďđēĕėęěĝğġģĥħĩīĭįıĵķĺļľŀłńņňŉŋōŏőoeŕŗřśŝsťŧ';
			$nonASCII.='ďđēĕėęěĝğġģĥħĩīĭįıĵķĺļľŀłńņňŉŋōŏőoeŕŗřśŝsťŧ';
			$nonASCII.='ũūŭůűųŵŷźżztșțΐάέήίΰαβγδεζηθικλμνξοπρςστυφ';
			$nonASCII.='χψωϊϋόύώабвгдежзийклмнопрстуфхцчшщъыьэюяt';
			$nonASCII.='ἀἁἂἃἄἅἆἇἐἑἒἓἔἕἠἡἢἣἤἥἦἧἰἱἲἳἴἵἶἷὀὁὂὃὄὅὐὑὒὓὔ';
			$nonASCII.='ὕὖὗὠὡὢὣὤὥὦὧὰάὲέὴήὶίὸόὺύὼώᾀᾁᾂᾃᾄᾅᾆᾇᾐᾑᾒᾓᾔᾕᾖᾗ';
			$nonASCII.='ᾠᾡᾢᾣᾤᾥᾦᾧᾰᾱᾲᾳᾴᾶᾷῂῃῄῆῇῐῑῒΐῖῗῠῡῢΰῤῥῦῧῲῳῴῶῷ';
			// note : 1 caractète non-ASCII vos 2 octets en UTF-8


			$syntaxe="#^[[:alnum:][:punct:]]{1,64}@[[:alnum:]-.$nonASCII]{2,253}\.[[:alpha:].]{2,6}$#";

			if(preg_match($syntaxe,$adresse))
				return true; // valide
			else
				return false; // pas valide
		}
    
		function envoyerMail($login, $mdp, $mail) {
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'To: ' . $login . ' <' . $mail . '>' . "\r\n";
			$headers .= 'From: webmaster@gestion_imputation.com' . "\r\n";
			
			$message = '
			<html>
				<head>
					<title>Confirmation de création de compte</title>
				</head>
				<body>
					<p> Votre compte a bien été crée sur le site www.gestion_imputation.com. Vos identifiants sont les suivants</p>
					<table>
						<tr>
							<td>Identifiant :</td>
							<td>' . $login . '</td>
						</tr>
						<tr>
							<td>Mot de passe :</td>
							<td>' . $login .  '</td>
						</tr>
					</table>
				</body>
			</html>
			';
	 
			mail($mail, 'Confirmation de création de compte', $message, $headers);
		}
		
	}