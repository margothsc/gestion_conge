<?php
	require_once 'Model.class.php';
	
	class Modele extends Model{
    
		function getOneModele($id) {
			$sql = 'SELECT * 
			        FROM modele
					WHERE id_modele = ?';
			
			$modele = $this->createRequest($sql, array($id));
	 
			if ($modele->rowCount() > 0)
				  return $modele->fetch(); 
			else
				return null;
		}
    
		function getByNom($nom) {
			$sql = 'SELECT * 
			        FROM modele
					WHERE libelle_modele = ?';
			
			$modele = $this->createRequest($sql, array($nom));
	 
			if ($modele->rowCount() > 0)
				  return $modele->fetch(); 
			else
				return null;
		}
		
		function getAllModele() {
			$sql = 'SELECT *
				    FROM modele';
			
			$modeles = $this->createRequest($sql);
	 
			if ($modeles->rowCount() > 0) 
				  return $modeles->fetchAll(); 
			else
				return null;
		}
		
		function getAllModeleSalarie($id) {
			$sql = 'SELECT DISTINCT m.id_modele, p.id_projet, p.libelle_projet
				    FROM modele m, salarie_projet sp, projet p
					WHERE m.id_projet = sp.id_projet
                    AND p.id_projet = m.id_projet
					AND sp.id_salarie = ?';
			
			$modeles = $this->createRequest($sql, array($id));
	 
			if ($modeles->rowCount() > 0) 
				  return $modeles->fetchAll(); 
			else
				return null;
		}
		
		function getAllChamps() {
			$sql = 'SELECT *
				    FROM champs';
			
			$champs = $this->createRequest($sql);
	 
			if ($champs->rowCount() > 0) 
				  return $champs->fetchAll(); 
			else
				return null;
		}
    
		function getOneModeleChamps($id) {
			$sql = 'SELECT * 
			        FROM modele_champs
					WHERE id_modele_champs = ?';
			
			$modele = $this->createRequest($sql, array($id));
	 
			if ($modele->rowCount() > 0)
				  return $modele->fetch(); 
			else
				return null;
		}
		
		function getAllModeleChamps($id) {
			$sql = 'SELECT *
				    FROM modele_champs mc, champs c
				    WHERE mc.id_champs = c.id_champs
				    AND id_modele = ?';
			
			$champs = $this->createRequest($sql, array($id));
	 
			if ($champs->rowCount() > 0) 
				  return $champs->fetchAll(); 
			else
				return null;
		}
		
		function addModele($nom) {
			$sql='INSERT INTO modele (libelle_modele)
				  VALUES (?)';
			
			$this->createRequest($sql, array($nom));
		}
		
		function editProjetModele($id_projet, $id_modele) {
			$sql='UPDATE modele
				  SET id_projet = ?
				  WHERE id_modele = ?';
			
			$this->createRequest($sql, array($id_projet, $id_modele));
		}
		
		function editProjetDejaModele($id_projet) {
			$sql='SELECT *
				  FROM modele
				  WHERE id_projet = ?';
			
			$deja = $this->createRequest($sql, array($id_projet));
	 
			if ($deja->rowCount() > 0) 
				  return $deja->fetch(); 
			else
				return null;
		}
		
		function addModeleChamps($type_activite, $type_champs, $decompte, $aide, $id_modele) {
			$sql='INSERT INTO modele_champs(type_activite, id_champs, decompte, aide, id_modele)
				  VALUES(?, ?, ?, ? ,?)';
			
			$this->createRequest($sql, array($type_activite, $type_champs, $decompte, $aide, $id_modele));
		}
		
		function editModeleChamps($type_activite, $type_champs, $decompte, $aide, $id_modele_champs) {
			$sql='UPDATE modele_champs
				  SET type_activite = ?,
				  id_champs = ?,
				  decompte = ?,
				  aide = ?
				  WHERE id_modele_champs = ?';
			
			$this->createRequest($sql, array($type_activite, $type_champs, $decompte, $aide, $id_modele_champs));
		}
		
		function deleteModeleChamps($id) {
			$sql='DELETE FROM modele_champs
				  WHERE id_modele_champs = ?';
			
			$this->createRequest($sql, array($id));
		}
		
		function isModele($id) {
			$sql = 'SELECT * 
			        FROM modele
					WHERE id_modele = ?';
			
			$modele = $this->createRequest($sql, array($id));
	 
			if ($modele->rowCount() > 0) 
				  return TRUE; 
			else
				return FALSE;
		}
		
		function getOneSalarieSaisie($id_salarie_saisie) {
			$sql = 'SELECT *
				    FROM salarie_saisie
				    WHERE id_salarie_saisie = ?';
			
			$salarie_saisie = $this->createRequest($sql, array($id_salarie_saisie));
	 
			if ($salarie_saisie->rowCount() > 0) 
				  return $salarie_saisie->fetch(); 
			else
				return null;
		}
		
		function getOneSalarieSaisieMois($id_salarie, $id_modele, $mois) {
			$sql = 'SELECT *
				    FROM salarie_saisie
				    WHERE id_modele = ?
					AND id_salarie = ?
					AND mois = ?';
			
			$salarie_saisie = $this->createRequest($sql, array($id_modele, $id_salarie, $mois));
	 
			if ($salarie_saisie->rowCount() > 0) 
				  return $salarie_saisie->fetch(); 
			else
				return null;
		}
		
		function getSalarieWithSaisie() {
			$sql = 'SELECT DISTINCT id_salarie
				    FROM salarie_saisie ss';
			
			$salarie_saisie = $this->createRequest($sql);
	 
			if ($salarie_saisie->rowCount() > 0) 
				  return $salarie_saisie->fetchAll(); 
			else
				return null;
		}
		
		function getAllSalarieSaisie() {
			$sql = 'SELECT *
				    FROM salarie_saisie ss, salarie s, modele m, projet p
				    WHERE m.id_modele = ss.id_modele
					AND s.id_salarie = ss.id_salarie
					AND p.id_projet = m.id_projet';
			
			$salarie_saisie = $this->createRequest($sql);
	 
			if ($salarie_saisie->rowCount() > 0) 
				  return $salarie_saisie->fetchAll(); 
			else
				return null;
		}
		
		function getAllSaisieBySalarie($id_salarie) {
			$sql = 'SELECT *
				    FROM salarie_saisie ss, salarie s
				    WHERE s.id_salarie = ss.id_salarie
					AND s.id_salarie = ?';
			
			$salarie_saisie = $this->createRequest($sql, array($id_salarie));
	 
			if ($salarie_saisie->rowCount() > 0) 
				  return $salarie_saisie->fetchAll(); 
			else
				return null;
		}
		
		function addSalarieSaisie($id_salarie, $id_modele, $mois) {
			$sql='INSERT INTO salarie_saisie(id_salarie, id_modele, mois)
				  VALUES(?, ?, ?)';
			
			$this->createRequest($sql, array($id_salarie, $id_modele, $mois));
		}
		
		function updateSalarieSaisie($total, $id_salarie_saisie) {
			$sql='UPDATE salarie_saisie
				  SET total = ?
				  WHERE id_salarie_saisie = ?';
			
			$this->createRequest($sql, array($total, $id_salarie_saisie));
		}
		
		function getOneSalarieChamps($id_salarie_champs) {
			$sql = 'SELECT *
				    FROM salarie_champs
				    WHERE id_salarie_champs';
			
			$champs = $this->createRequest($sql, array($id_salarie_champs));
	 
			if ($champs->rowCount() > 0) 
				  return $champs->fetch(); 
			else
				return null;
		}
		
		function getAllSalarieChamps($id_salarie_saisie) {
			$sql = 'SELECT *
				    FROM salarie_champs
				    WHERE id_salarie_saisie = ?';
			
			$champs = $this->createRequest($sql, array($id_salarie_saisie));
	 
			if ($champs->rowCount() > 0) 
				  return $champs->fetchAll(); 
			else
				return null;
		}
		
		function addSalarieChamps($id_salarie_saisie, $type_activite, $decompte, $aide) {
			$sql='INSERT INTO salarie_champs(type_activite, decompte, aide, id_salarie_saisie)
				  VALUES(?, ?, ?, ?)';
			
			$this->createRequest($sql, array($type_activite, $decompte, $aide, $id_salarie_saisie));
		}
		
		function updateSalarieChamps($nbr_jours, $commentaire, $id_salarie_champs) {
			$sql='UPDATE salarie_champs
				  SET nbr_jours = ?,
				  commentaire = ?
				  WHERE id_salarie_champs = ?';
			
			$this->createRequest($sql, array($nbr_jours, $commentaire, $id_salarie_champs));
		}
		
		function deleteSalarieChamps($id_salarie_champs) {
			$sql='DELETE FROM salarie_champs
				  WHERE id_salarie_champs = ?';
			
			$this->createRequest($sql, array($id_salarie_champs));
		}
	}