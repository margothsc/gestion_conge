<?php
	require_once 'Model.class.php';
	
	class Salarie extends Model{
    
		function getOneSalarie($id) {
			$sql = 'SELECT * 
			        FROM salarie
					WHERE id_salarie = ?';
			
			$user = $this->createRequest($sql, array($id));
	 
			if ($user->rowCount() > 0)
				  return $user->fetch(); 
			else
				return null;
		}
    
		function getOneSalarieUtilisateur($id) {
			$sql = 'SELECT * 
			        FROM salarie
					WHERE id_utilisateur = ?';
			
			$user = $this->createRequest($sql, array($id));
	 
			if ($user->rowCount() > 0)
				  return $user->fetch(); 
			else
				return null;
		}
		
		function getAllSalarie() {
			$sql = 'SELECT *
				    FROM salarie';
			
			$user = $this->createRequest($sql);
	 
			if ($user->rowCount() > 0) 
				  return $user->fetchAll(); 
			else
				return null;
		}
		
		function getSalarieWithProject() {
			$sql = 'SELECT *
				    FROM salarie s, projet p, salarie_projet sp
				    WHERE sp.id_projet = p.id_projet
				    AND sp.id_salarie = s.id_salarie';
			
			$user = $this->createRequest($sql);
	 
			if ($user->rowCount() > 0) 
				  return $user->fetchAll(); 
			else
				return null;
		}
		
		function addSalarie($code, $nom, $prenom, $mail, $id_utilisateur) {
			$sql='INSERT INTO salarie(code, nom, prenom, mail, id_utilisateur)
				  VALUES(?, ?, ?, ?, ?)';
			
			$this->createRequest($sql, array($code, $nom, $prenom, $mail, $id_utilisateur));
		}
		
		function deleteSalarie($id) {
			$sql='DELETE FROM salarie
				  WHERE id_salarie = ?';
			
			$this->createRequest($sql, array($id));
		}
		
		function editFicheSalarie($id, $code, $nom, $prenom, $mail) {
			$sql='UPDATE salarie
				  SET code = ?,
				  nom = ?,
				  prenom = ?,
				  mail = ?
				  WHERE id_salarie = ?';
			
			$this->createRequest($sql, array($code, $nom, $prenom, $mail, $id));
		}
		
		function getProjectSalarie($id) {
			$sql = 'SELECT * 
			        FROM projet p, salarie_projet sp
					WHERE id_salarie = ?
					AND p.id_projet = sp.id_projet';
			
			$projets = $this->createRequest($sql, array($id));
	 
			if ($projets->rowCount() > 0)
				  return $projets->fetchAll(); 
			else
				return null;
		}
		
		function getProjetSalarieFini($id_salarie) {
			$date = date(DATE_W3C);
			
			$sql = 'SELECT *
				    FROM salarie s, projet p, salarie_projet sp
				    WHERE sp.id_projet = p.id_projet
				    AND sp.id_salarie = s.id_salarie
					AND sp.date_fin < ?
					AND s.id_salarie = ?';
			
			$user = $this->createRequest($sql, array($date, $id_salarie));
	 
			if ($user->rowCount() > 0) 
				  return $user->fetchAll(); 
			else
				return null;
		}
		
		function addProjetSalarie($id_salarie, $id_projet, $debut, $fin) {
			$sql='INSERT INTO salarie_projet
				  VALUES(?, ?, ?, ?)';
			
			$this->createRequest($sql, array($id_salarie, $id_projet, $debut, $fin));
		}
		
		function editProjetSalarie($id_salarie, $id_projet, $debut, $fin) {
			$sql='UPDATE salarie_projet
				  SET date_debut = ?,
				  date_fin = ?
				  WHERE id_salarie = ?
				  AND id_projet = ?';
			
			$this->createRequest($sql, array($debut, $fin, $id_salarie, $id_projet));
		}
		
		function deleteProjetSalarie($id_salarie, $id_projet) {
			$sql='DELETE FROM salarie_projet
				  WHERE id_salarie = ?
				  AND id_projet = ?';
			
			$this->createRequest($sql, array($id_salarie, $id_projet));
		}
		
		function existe($id) {
			$sql = 'SELECT * 
			        FROM salarie
					WHERE id_salarie = ?';
			
			$user = $this->createRequest($sql, array($id));
	 
			if ($user->rowCount() > 0) 
				  return TRUE; 
			else
				return FALSE;
		}
		
		function projetExiste($id_projet, $id_salarie) {
			$sql = 'SELECT * 
				    FROM salarie s, salarie_projet sp
				    WHERE sp.id_salarie = s.id_salarie
					AND sp.id_projet = ?
					AND s.id_salarie = ?';
			
			$projet = $this->createRequest($sql, array($id_projet, $id_salarie));
	 
			if ($projet->rowCount() > 0) 
				  return TRUE; 
			else
				return FALSE;
		}
	}