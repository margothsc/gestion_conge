<?php
	require_once dirname(__FILE__). '/../config/config.php';

	class Model {
		private $bdd;
	 
		private function createConnexion(){
				try{
					   $this->bdd = new PDO('mysql:host='.HOST.';dbname='.BDD.'', ''.LOGIN.'', ''.PASS.'');
					   $this->bdd->exec("SET CHARACTER SET utf8");
					}
				catch(Exception $e){
				 
							die('Erreur : '.$e->getMessage());
					}
			return $this->bdd;
		}
	 
		protected function createRequest($sql,$params=null){
			if($params==null){
					$result = $this->createConnexion()->prepare($sql); 
					$result->execute();
					}
					else{
						$result = $this->createConnexion()->prepare($sql);
						$result->execute($params);
					}
					return $result;
		}
	
		function getURLofPath($pathname) {
			// Nettoyage du pathname
			$realpath = realpath($pathname);
			$realpath = str_replace('\\','/',$realpath);
			
			// Comparaison au document Root
			$rootpath = str_replace('\\','/',$_SERVER["DOCUMENT_ROOT"]);
			if(substr($realpath, 0, strlen($rootpath)) != $rootpath)
				return $pathname;
			
			// Construction de l'URL absolue
			return 'http://'.$_SERVER["SERVER_NAME"].'/'.substr($realpath, strlen($rootpath));
		}
	}