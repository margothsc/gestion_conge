<?php
	require_once 'Model.class.php';

	class Projet extends Model{
		
		function getOneProjet($id) {
			$sql = 'SELECT *
				    FROM projet
					WHERE id_projet = ?';
			
			$projets = $this->createRequest($sql, array($id));
	 
			if ($projets->rowCount() > 0) 
				  return $projets->fetch(); 
			else
				return null;
		}
		
		function getAllProject() {
			$sql = 'SELECT *
				    FROM projet';
			
			$projets = $this->createRequest($sql);
	 
			if ($projets->rowCount() > 0) 
				  return $projets->fetchAll(); 
			else
				return null;
		}
		
		function addProject($libelle) {
			$sql='INSERT INTO projet(libelle_projet)
				  VALUES(?)';
			
			$this->createRequest($sql, array($libelle));
		}
		
		function editProject($id, $libelle) {
			$sql='UPDATE projet
				  SET libelle_projet = ?
				  WHERE id_projet = ?';
			
			$this->createRequest($sql, array($libelle, $id));
		}
		
		function deleteProject($id) {
			$sql='DELETE FROM projet
				  WHERE id_projet = ?';
			
			$this->createRequest($sql, array($id));
		}
	}