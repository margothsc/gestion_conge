<h1> Gestion des imputations </h1>

<section id="connexion">
	<p id="erreur"> <?php echo $strErrorMsg ?> </p>
	<form id="form_login" method="post">
		<input type="text" name="identifiant" placeholder="Identifiant" autofocus required> </br>
		<input type="password" id="mdp" name="mdp" placeholder="Mot de passe" required> </br>
		<input type="hidden" id="passCrypt" name="passCrypt" value="" />
		<input type="hidden" id="graine" value="<?php echo $_SESSION['seed'] ?>" />
		<input type="submit" value="Connexion">
	</form>
	<p> Mot de passe oublié ? </p>
</section>