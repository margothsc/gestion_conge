<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
		<link rel="stylesheet" href="<?php echo $_SERVER['path'] ?>/../design/resetcss.css">
		<link rel="stylesheet" href="<?php echo $_SERVER['path'] ?>/../design/gestion.css"> 
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/dark-hive/jquery-ui.css">
        <title> Gestion des imputations </title>
    </head>
	<body>
		<?php if(isset($_SESSION['utilisateur']['login'])) { ?>
		<section id="en_tete">
			<h2> Gestion des imputations </h2>
			<aside id="identifiant"> 
				<h3 class="change"> <?php echo $_SESSION['utilisateur']['login'] ?> <img src="<?php echo $_SERVER['path'] ?>/../design/asc.gif"> </h3>  
				<a class="logout"> Déconnexion </a> 
			</aside>
		</section>
		<aside class="changeMdp" style="display:none">
			<form id="form_changement" method="post">
				<p id="changement"> </p>
				<input type="password" id="oldMdp" placeholder="Ancien mot de passe" autofocus required> </br>
                <input type="hidden" id="oldPassCrypt" name="passCrypt" value="" />
				<p id="different" style="display:none"> Les mots de passe saisis sont différents </p>
				<p id="invalide" style="display:none"> Pour être valide, votre mot de passe doit compter au minimum 6 caractères. </p>
				<input type="password" id="mdp" placeholder="Nouveau mot de passe" required> </br>
				<input type="password" id="mdpBis" placeholder="Confirmez le mot de passe" required> </br>
                <input type="hidden" id="passCrypt" name="passCrypt" value="" />
				<input type="submit" value="Changer">
			</form>
		</aside>
		<?php } ?>