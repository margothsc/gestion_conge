<form method="post" class="choix">
	<fieldset>
		<label> Projet en cours </label> 
		<select id="choix">
			<?php foreach($listeModele as $liste) { ?>
			<option class="choixModeleSaisie" id-modele="<?php echo $liste['id_modele'] ?>" <?php if($liste["id_projet"] == $modele["id_projet"]) echo 'selected' ?>> <?php echo $liste['libelle_projet'] ?> </option>
			<?php } ?>
		</select> 
	</fieldset>
</form>
<section id="saisie">
	<section id="saisie2" id-modele="<?php echo $modele['id_modele'] ?>" id-saisie="<?php echo $saisie['id_salarie_saisie'] ?>">
		<label for="nom"> Nom </label> <input type="text" id="nom" value="<?php echo $salarie['nom'] ?>" disabled> </br>
		<label for="prenom"> Prénom </label> <input type="text" id="prenom" value="<?php echo $salarie['prenom'] ?>" disabled> </br>
	<label for="mois"> Mois </label>  <input type="date" id="mois" value="<?php echo $saisie['mois'] ?>">
		<table>
			<thead>
				<tr> 
					<td colspan=5> Relevé d'activité </td>
				</tr>
				<tr>
					<td> Type d'activité </td>
					<td> Nombre de jours </td>
					<td class="com"> Commentaire </td>
					<td class="com"> Aide </td>
					<td> Actions </td>
				</tr>
				<?php if(isset($listeChamps)) { ?>
					<?php foreach($listeChamps as $champs) { ?>
						<tr class="<?php echo $champs['id_salarie_champs'] ?>">
							<td> <?php echo $champs['type_activite'] ?> </td>
							<td> <input type="text" class="nbr_jours" value="<?php echo $champs['nbr_jours'] ?>"> </td>
							<td> <textarea class="commentaire"><?php echo $champs['commentaire'] ?></textarea> </td>
							<td> <?php echo $champs['aide'] ?> </td>
							<td> <button type="button" class="valider"> Valider </button> </br>
								<button type="button" class="supprimer"> Supprimer </button>
							</td>
						</tr>
					<?php } ?>
				<?php } ?>
				<tr>
					<td> Total </td>
					<td> <?php echo $total ?> </td>
				</tr>
			</thead>
		</table>
	</section>
</section>