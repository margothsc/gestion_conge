<table class="tableSorter projet">
	<thead>
		<tr>
			<th> Libellé </th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($projets as $projet) { ?>					
			<tr id="line<?php echo $projet['id_projet']?>">
				<td> <span class="text_line"> <?php echo $projet['libelle_projet'] ?>  </span>
					<input style="display:none" type="text" class="input_line nom" value="<?php echo $projet['libelle_projet'] ?>">
				</td>
				<td> <button type="button" class="modifier" line-id="<?php echo $projet['id_projet']?>"> Modifier </button> </br> 
					<button type="button" class="supprimer" line-id="<?php echo $projet['id_projet']?>"> Supprimer </button> 
					<button type="button" class="valider" line-id="<?php echo $projet['id_projet']?>" style="display:none"> Valider </button>
				</td>
			</tr>
		<?php } ?>			
		<tr>
			<td> <input type="text" id="newNom"> </td>
			<td> <button type="button" id="ajouter_projet"> Ajouter</button> </td>
		</tr>	
	</tbody>
</table>