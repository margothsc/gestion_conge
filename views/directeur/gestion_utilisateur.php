<table class="tableSorter utilisateur">
	<thead>
		<tr>
			<th> Login </th>
			<th> Mail </th>
			<th> Droit </th>
			<th> Date d'inscription </th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($utilisateurs as $utilisateur) { ?>					
			<tr id="line<?php echo $utilisateur['id_utilisateur'] ?>">
				<td> <span class="text_line"> <?php echo $utilisateur['login'] ?>  </span>
					<input style="display:none" type="text" class="input_line log" value="<?php echo $utilisateur['login'] ?>">
				</td>
				<td> <span class="text_line"> <?php echo $utilisateur['mail'] ?>  </span>
					<input style="display:none" type="text" class="input_line mail" value="<?php echo $utilisateur['mail'] ?>">
				</td>
				<td> <span class="text_line"> <?php echo $utilisateur['role'] ?> </span>
					<select style="display:none" class="input_line role">
						<?php foreach($roles as $role) { ?>
							<option <?php if($utilisateur['role'] == $role['role']) echo 'selected' ?> value="<?php echo $role['role'] ?>"> <?php echo $role['role'] ?> </option>
						<?php } ?>
					</select>
				</td>
				<td> <?php echo strftime('%d-%m-%Y', strtotime($utilisateur['date_inscription'])) ?> </td>
				<td> <button type="button" class="modifier" line-id="<?php echo $utilisateur['id_utilisateur']?>"> Modifier </button> </br> 
					<button type="button" class="supprimer" line-id="<?php echo $utilisateur['id_utilisateur']?>"> Supprimer </button> 
					<button type="button" class="valider" line-id="<?php echo $utilisateur['id_utilisateur']?>" style="display:none"> Valider </button>
				</td>
			</tr>
		<?php } ?>			
		<tr>
			<td> <input type="text" id="newLog"> </td>
			<td> <input type="text" id="newMail"> </td>
			<td> <select id="newRole">
					<?php foreach($roles as $role) { ?>
						<option value="<?php echo $role['role'] ?>"> <?php echo $role['role'] ?> </option>
					<?php } ?>
				 </select>
			</td>
			<td> <?php echo date("d-m-Y") ?> </td>
			<td> <button type="button" id="ajouter_utilisateur"> Ajouter </button> </td>
		</tr>	
	</tbody>
</table>