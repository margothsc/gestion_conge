<section id="modele" id-modele="<?php echo $modele['id_modele'] ?>">
	<form method="post">
		<fieldset>
			<label for="nom"> Nom </label> <input type="text" id="nom" disabled> </br>
			<label for="prenom"> Prénom </label> <input type="text" id="prenom" disabled> </br>
			<label for="mois"> Mois </label>  <input type="date" id="mois" disabled>
		</fieldset>
	</form>
	<table>
		<thead>
			<tr> 
				<td colspan=5> Relevé d'activité </td>
			</tr>
			<tr>
				<td> Type d'activité </td>
				<td> Nombre de jours </td>
				<td class="com"> Commentaire </td>
				<td class="com"> Aide </td>
				<td> Actions </td>
			</tr>
			<?php if(isset($listeModeleChamps)) { ?>
				<?php foreach($listeModeleChamps as $modeleChamps) { ?>
				<tr id="<?php echo $modeleChamps['id_modele_champs']?>">
					<td> <span class="text_line"> <?php echo $modeleChamps['type_activite'] ?>  </span>
						<input style="display:none" type="text" class="input_line type_activite" value="<?php echo $modeleChamps['type_activite'] ?>">
					</td>
					<td> <span class="text_line"> <?php echo $modeleChamps['type'] ?>  </span>
						<select class="input_line type" style="display:none"> 
							<?php foreach($listeChamps as $champs) { ?>
							<option value="<?php echo $champs['id_champs'] ?>" <?php if($champs['type'] == $modeleChamps['type']) echo 'selected' ?>> <?php echo $champs['type'] ?> </option>
							<?php } ?>
						</select>
						<input class="decompte" type="checkbox" <?php if($modeleChamps['decompte'] == 1) echo 'checked' ?> disabled> 
					</td>
					<td> </td>
					<td> <span class="text_line"> <?php echo $modeleChamps['aide'] ?>  </span>
						<textarea style="display:none" class="input_line aide"> <?php echo $modeleChamps['aide'] ?> </textarea>
					</td>
				<td> <button type="button" class="modifier"> Modifier </button> <button type="button" class="valider" style="display:none" id="<?php echo $modeleChamps['id_modele_champs'] ?>"> Valider </button> </br>
					<button type="button" class="supprimer" id="<?php echo $modeleChamps['id_modele_champs'] ?>"> Supprimer </button>
				</td>
				</tr>
				<?php } ?>
			<?php } ?>
			<tr>
				<td> <input type="text" id="type_activite"> </td>
				<td> 
					<select id="type_champs"> 
						<?php foreach($listeChamps as $champs) { ?>
						<option value="<?php echo $champs['id_champs'] ?>"> <?php echo $champs['type'] ?> </option>
						<?php } ?>
					</select>
					<input type="checkbox" id="decompte" value=1> 
				</td>
				<td> <textarea id="com" disabled> </textarea> </td>
				<td> <textarea id="aide"> </textarea> </td>
				<td> <button type="button" id="nouveau_champs"> Ajouter </button> </td>
			</tr>
			<tr>
				<td> Total </td>
				<td> <input type="text" id="total" disabled> </td>
			</tr>
		</thead>
	</table>
</section>