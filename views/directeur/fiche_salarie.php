<button type="button" class="modifier_salarie"> Modifier la fiche </button>
<button type="button" class="valider_salarie" line-id="<?php echo $salarie['id_salarie']?>" style="display:none"> Valider la fiche </button>
<button type="button" class="supprimer_salarie" line-id="<?php echo $salarie['id_salarie']?>"> Supprimer la fiche </button>
<section id="fiche">
	<table>
		<tr class="salarie">
			<td class="titre"> Code </td>
			<td> <span class="text_line"> <?php echo $salarie['code'] ?>  </span>
				<input style="display:none" type="text" class="input_line code" value="<?php echo $salarie['code'] ?>">
			</td>
		</tr>
		<tr class="salarie">
			<td class="titre"> Nom </td>
			<td> <span class="text_line"> <?php echo $salarie['nom'] ?>  </span>
				<input style="display:none" type="text" class="input_line nom" value="<?php echo $salarie['nom'] ?>">
			</td>
		</tr>
		<tr class="salarie">
			<td class="titre"> Prénom </td>
			<td> <span class="text_line"> <?php echo $salarie['prenom'] ?>  </span>
				<input style="display:none" type="text" class="input_line prenom" value="<?php echo $salarie['prenom'] ?>">
			</td>
		</tr>
		<tr class="salarie">
			<td class="titre"> Adresse e-mail </td>
			<td> <span class="text_line"> <?php echo $salarie['mail'] ?>  </span>
				<input style="display:none" type="text" class="input_line mail" value="<?php echo $salarie['mail'] ?>">
			</td>
		</tr>
		<?php if(isset($projets)) {
			foreach($projets as $projet) { ?>
				<tr class="projet_salarie<?php echo $projet['id_projet']?>" id-salarie="<?php echo $salarie['id_salarie']?>" id-projet="<?php echo $projet['id_projet']?>">
					<td> 
						<button type="button" class="modifier"> Modifier le projet </button>
						<button type="button" class="valider" style="display:none"> Valider </button> 
					</td>
					<td> 
						<button type="button" class="supprimer"> Supprimer le projet </button> 
					</td>
				</tr>
				<tr class="projet_salarie<?php echo $projet['id_projet']?>">
					<td class="titre"> Projet </td>
					<td> <span class="text_line"> <?php echo $projet['libelle_projet'] ?>  </span>
						<input style="display:none" type="text" class="input_line libelle" value="<?php echo $projet['libelle_projet'] ?>" disabled>
					</td>
				</tr>
				<tr class="projet_salarie<?php echo $projet['id_projet']?>">
					<td class="titre"> Date de début </td>
					<td> <span class="text_line"> <?php echo strftime('%d-%m-%Y', strtotime($projet['date_debut'])) ?>  </span>
						<input style="display:none" type="text" class="input_line debut" value="<?php echo strftime('%d-%m-%Y', strtotime($projet['date_debut'])) ?>">
					</td>
				</tr>
				<tr class="projet_salarie<?php echo $projet['id_projet'] ?>">
					<td class="titre"> Date de fin </td>
					<td> <span class="text_line"> <?php echo strftime('%d-%m-%Y', strtotime($projet['date_fin'])) ?>  </span>
						<input style="display:none" type="text" class="input_line fin" value="<?php echo strftime('%d-%m-%Y', strtotime($projet['date_fin'])) ?>">
					</td>
				</tr>
			<?php } 
		} ?>
	</table>
	<table id="newProjet">
		<?php if(isset($message)) { ?>
			<p> <?php echo $message ?> </p>
		<?php } ?>
		<tr>
			<td class="titre"> Projet </td>
			<td> <?php if(isset($liste_projets)) { ?>
				<select class="libelle"> 
					<?php foreach($liste_projets as $projet) { ?> 
						<option value="<?php echo $projet['id_projet'] ?>"> <?php echo $projet['libelle_projet'] ?> </option> 
					<?php } ?>  
				</select>
				<?php } ?>
			</td>
		</tr>
		<tr>
			<td class="titre"> Date de début </td>
			<td> <input type="text" placeholder="dd-mm-yyyy" class="debut"> </td>
		</tr>
		<tr>
			<td class="titre"> Date de fin </td>
			<td> <input type="text" placeholder="dd-mm-yyyy" class="fin"> </td>
		</tr>
		<tr>
			<td> <input type="hidden" class="id-salarie" value="<?php echo $salarie['id_salarie']?>"> 
			<button type="button" id="nouveau_projet_salarie"> Ajouter le projet </button> </td>
		</tr>
	</table>
</section>
