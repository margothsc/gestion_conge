<?php if(isset($listeSalarieSaisie)) { ?>
	<table class="tableSorter" id="listeSalarie">
		<thead>
			<tr>	
				<th> Nom </th>
				<th> Prénom </th>
				<th> Modèle </th>
				<th> Pour le mois de </th>
				<th> Projet lié </th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($listeSalarieSaisie as $salarie){ ?>
			<tr>
				<td> <?php echo $salarie['nom'] ?> </td>
				<td> <?php echo $salarie['prenom'] ?> </td>
				<td> <?php echo $salarie['libelle_modele'] ?> </td>
				<td> <?php echo $salarie['mois'] ?> </td>
				<td> <?php echo $salarie['libelle_projet'] ?> </td>
				<td> <a href="controller/exporter_imputation.php?id_salarie=<?php echo $salarie['id_salarie'] ?>&id_salarie_saisie=<?php echo $salarie['id_salarie_saisie'] ?>"><button type="button"> Exporter </button> </a> </td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	
	<a href="controller/exporter_imputation_all.php"> <button type="button"> Exporter tout </button> </a>
<?php } ?>