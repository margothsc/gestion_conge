<?php
if(!isset($_SESSION))
	session_start();  

require_once 'controller/ControllerView.class.php';
require_once 'controller/ControllerConge.class.php';
require_once 'controller/ControllerModele.class.php';
require_once 'controller/ControllerProjet.class.php';
require_once 'controller/ControllerSalarie.class.php';
require_once 'controller/ControllerUtilisateur.class.php';
require_once 'model/Model.class.php';
require_once 'model/Conge.class.php';
require_once 'model/Modele.class.php';
require_once 'model/Projet.class.php';
require_once 'model/Salarie.class.php';
require_once 'model/Utilisateur.class.php';

class Controller {
    private $model;
    private $modelConge;
    private $modelModele;
    private $modelProjet;
    private $modelSalarie;
    private $modelUtilisateur;
    private $ctrlConge;
    private $ctrlModele;
    private $ctrlProjet;
    private $ctrlSalarie;
    private $ctrlUtilisateur;
    private $ctrlView;
    
    // Constructeur
    function Controller() 
	{
        $this->model = new Model();
        $this->modelConge = new Conge();
        $this->modelModele = new Modele();
        $this->modelProjet = new Projet();
        $this->modelSalarie = new Salarie();
        $this->modelUtilisateur = new Utilisateur();
        $this->ctrlView = new ControllerView();
        $this->ctrlConge = new ControllerConge($this->modelConge, $this->modelProjet);
        $this->ctrlModele = new ControllerModele($this->modelModele, $this->modelProjet, $this->modelSalarie);
        $this->ctrlProjet = new ControllerProjet($this->modelProjet);
        $this->ctrlSalarie = new ControllerSalarie($this->modelSalarie, $this->modelProjet, $this->modelUtilisateur);
        $this->ctrlUtilisateur = new ControllerUtilisateur($this->modelUtilisateur);
        $_SERVER['path'] = $this->model->getURLofPath(dirname(__FILE__));
    }
    
    // Point d'entrée générique du controleur
    function dispatcher() 
	{
		if(isset($_REQUEST['action'])) {
			if($_SESSION['role'] == 'salarie') {
				if($this->modelSalarie->existe($_REQUEST['action']))
					return $this->onDroitInsuffisant();
				switch($_GET['action'])
				{
					case 'changer_mois_saisie' :
						$this->ctrlModele->onChangerMoisSaisie();
						break;
					case 'changer_modele_saisie' :
						$this->ctrlModele->onChangerModeleSaisie();
						break;
					case 'ajouter_modele_saisie' :
						$this->ctrlModele->onAjouterModeleSaisie();
						break;
					case 'valider_modele_saisie' :
						$this->ctrlModele->onValiderModeleSaisie();
						break;
					case 'supprimer_modele_saisie' :
						$this->ctrlModele->onSupprimerModeleSaisie();
						break;
					case 'consulter_imputation' :
						$this->ctrlModele->onConsultImputation();
						break;
					case 'modele_imputation' :
					case 'changer_modele_imputation' :
					case 'nouveau_modele_imputation' :
					case 'lier_modele_imputation' :
					case 'copier_modele_imputation' :
					case 'nouveau_modele_champs' :
					case 'modifier_modele_champs' :
					case 'supprimer_modele_champs' :
					case 'consulter_fichier_conge' :
					case 'changer_fichier_conge' :
					case 'ajouter_fichier_conge' :
					case 'importer_fichier_conge' :
					case 'gestion_salarie' :
					case 'nouveau_salarie' :
					case 'modifier_salarie' :
					case 'supprimer_salarie' :
					case 'nouveau_projet_salarie' :
					case 'modifier_projet_salarie' :
					case 'supprimer_projet_salarie' :
					case 'modifier_fiche_salarie' :
					case 'supprimer_projet_fini' :
					case 'envoyer_mail_rappel' :
					case 'gestion_projet' :
					case 'nouveau_projet' :
					case 'modifier_projet' :
					case 'supprimer_projet' :
					case 'gestion_utilisateur' :
					case 'nouvel_utilisateur' :
					case 'modifier_utilisateur' :
					case 'supprimer_utilisateur' :
					case 'changerMdp' :
						$this->onDroitInsuffisant();
						break;
					default :
						$this->onMethodNotImplemented();
						break;
				}
			}
			else {
				if($this->modelSalarie->existe($_REQUEST['action']))
					return $this->ctrlSalarie->onSalarie();
				switch($_GET['action'])
				{
					case 'changer_mois_saisie' :
					case 'changer_modele_saisie' :
					case 'ajouter_modele_saisie' :
					case 'valider_modele_saisie' :
					case 'supprimer_modele_saisie' :
						$this->onPageSalarie();
						break;
					case 'consulter_imputation' :
						$this->ctrlModele->onConsultImputation();
						break;
					case 'modele_imputation' :
						$this->ctrlModele->onModeleImputation(1);
						break;
					case 'changer_modele_imputation' :
						$this->ctrlModele->onChangerModeleImputation();
						break;
					case 'nouveau_modele_imputation' :
						$this->ctrlModele->onNouveauModeleImputation();
						break;
					case 'lier_modele_imputation' :
						$this->ctrlModele->onLierModeleImputation();
						break;
					case 'copier_modele_imputation' :
						$this->ctrlModele->onCopierModeleImputation();
						break;
					case 'nouveau_modele_champs' :
						$this->ctrlModele->onNouveauModeleChamps();
						break;
					case 'modifier_modele_champs' :
						$this->ctrlModele->onModifierModeleChamps();
						break;
					case 'supprimer_modele_champs' :
						$this->ctrlModele->onSupprimerModeleChamps();
						break;
					case 'consulter_fichier_conge' :
						$this->ctrlConge->onConsultConge();
						break;
					case 'changer_fichier_conge' :
						$this->ctrlConge->onChangerConge();
						break;
					case 'ajouter_fichier_conge' :
						$this->ctrlConge->onNouveauFichierConge();
						break;
					case 'importer_fichier_conge' :
						$this->ctrlConge->onImporterFichierConge();
						break;
					case 'gestion_salarie' :
						$this->ctrlSalarie->onGestionSalarie();
						break;
					case 'nouveau_salarie' :
						$this->ctrlSalarie->onNouveauSalarie();
						break;
					case 'modifier_salarie' :
						$this->ctrlSalarie->onModifierSalarie();
						break;
					case 'supprimer_salarie' :
						$this->ctrlSalarie->onSupprimerSalarie();
						break;
					case 'nouveau_projet_salarie' :
						$this->ctrlSalarie->onNouveauProjetSalarie();
						break;
					case 'modifier_projet_salarie' :
						$this->ctrlSalarie->onModifierProjetSalarie();
						break;
					case 'supprimer_projet_salarie' :
						$this->ctrlSalarie->onSupprimerProjetSalarie();
						break;
					case 'modifier_fiche_salarie' :
						$this->ctrlSalarie->onModifierFicheSalarie();
						break;
					case 'supprimer_projet_fini' :
						$this->ctrlSalarie->onSupprimerProjetFini();
						break;
					case 'envoyer_mail_rappel' :
						$this->ctrlSalarie->onEnvoyerMailRappel();
						break;
					case 'gestion_projet' :
						$this->ctrlProjet->onGestionProjet();
						break;
					case 'nouveau_projet' :
						$this->ctrlProjet->onNouveauProjet();
						break;
					case 'modifier_projet' :
						$this->ctrlProjet->onModifierProjet();
						break;
					case 'supprimer_projet' :
						$this->ctrlProjet->onSupprimerProjet();
						break;
					case 'gestion_utilisateur' :
						$this->ctrlUtilisateur->onGestionUtilisateur();
						break;
					case 'nouvel_utilisateur' :
						$this->ctrlUtilisateur->onNouvelUtilisateur();
						break;
					case 'modifier_utilisateur' :
						$this->ctrlUtilisateur->onModifierUtilisateur();
						break;
					case 'supprimer_utilisateur' :
						$this->ctrlUtilisateur->onSupprimerUtilisateur();
						break;
					case 'changerMdp' :
						$this->ctrlUtilisateur->onModifierMdpUtilisateur();
						break;
					default :
						$this->onMethodNotImplemented();
						break;
				}
			}
		}
		else $this->onLog();
    }

	function Authorize() {
		if((isset($_REQUEST['reason']))&&($_REQUEST['reason'] == 'logout'))
			$this->modelUtilisateur->hard_session_destroy();

		// si l'utilisateur vient d'arriver
		if(count($_SESSION) == 0) 
			return $this->onStart();
		// sinon si l'utilisateur est en train de se logger
		if(!$this->modelUtilisateur->isLoggedIn() && isset($_SESSION['seed']))
		{
			if(isset($_REQUEST['identifiant']) && isset($_REQUEST['mdp'])) {
				$login = $_REQUEST['identifiant'];
				if(($tUserDetails = $this->modelUtilisateur->getOneUtilisateur($login)) === FALSE)
					return $this->onStart('L\'identifiant n\'existe pas');

				$mdp_encrypt = $this->modelUtilisateur->Encrypt($tUserDetails['mot_de_passe'], $_SESSION['seed']);
				if($mdp_encrypt != $_REQUEST['passCrypt'])
					return $this->onStart('Le mot de passe est incorrect');

				unset($tUserDetails['mot_de_passe']);
				$_SESSION['utilisateur'] = $tUserDetails;
				$_SESSION['role'] = $tUserDetails['role'];
				return $this->onLog();
			}
		}

		if(!$this->modelUtilisateur->isLoggedIn())
		{
			$this->modelUtilisateur->hard_session_destroy();
			return $this->onStart();
		}
		return TRUE;
	}
	
	function onStart($strErrorMsg = '') {
		if(!isset($_SESSION['seed']))
			$_SESSION['seed'] = rand(64,1024);
		
		$this->header();
		include(dirname(__FILE__).'/../views/connexion.php');
		$this->footer();
	}
    
    function onLog() {
		$this->ctrlModele->onConsultImputation();
    }
    // Appelée lorsque l'utilisateur génère un évènement non supporté
    function onMethodNotImplemented() 
	{
		$this->header();
		include(dirname(__FILE__).'/../views/erreur/erreur404.php');
		$this->footer();
    }
    
    function onDroitInsuffisant() {
		
		$this->header();
		include(dirname(__FILE__).'/../views/erreur/droit_insuffisant.php');
		$this->footer();
    }
    
    function onPageSalarie() {
		
		$this->header();
		include(dirname(__FILE__).'/../views/erreur/page_salarie.php');
		$this->footer();
    }
	
	function header($nb = 1) {
		include (dirname(__FILE__).'/../views/template.php');
		if(isset($_SESSION['utilisateur']['login'])) {
			echo '
			<aside id="menu">
				<a class="menu '.($nb == 1?'select':'').'" href="?action=consulter_imputation"> Imputation opérationnelle </a>';
			if($_SESSION['role'] == 'directeur') {
				echo '
				<a class="menu '.($nb == 2?'select':'').'" href="?action=modele_imputation"> Modèle d\'imputation </a>
				<a class="menu '.($nb == 3?'select':'').'" href="?action=consulter_fichier_conge"> Fichier congé </a>
				<a class="menu '.($nb == 4?'select':'').'" href="?action=ajouter_fichier_conge"> Ajouter fichier congé </a>
				<a class="menu '.($nb == 5?'select':'').'" href="?action=gestion_salarie"> Gestion des salariés </a>
				<a class="menu '.($nb == 6?'select':'').'" href="?action=gestion_projet"> Gestion des projets </a>
				<a class="menu '.($nb == 7?'select':'').'" href="?action=gestion_utilisateur"> Gestion des utilisateurs </a>';
			}
			echo '
			</aside>
			<section id="page">';
		}
	}
	
	function footer() {
		include (dirname(__FILE__).'/../views/pied_page.php');
	}
}