<?php
require_once('model/Salarie.class.php');

class ControllerSalarie extends Controller {
    private $oModel;
    private $modelProjet;
    private $modelUtilisateur;

    
    // Constructeur
    function ControllerSalarie($modelSalarie, $modelProjet, $modelUtilisateur) 
	{
        $this->oModel = $modelSalarie;
        $this->modelProjet = $modelProjet;
        $this->modelUtilisateur = $modelUtilisateur;
    }

    function onAfficherPage() 
    {
        $gSalarie = $this->oModel->getAllSalarie();
		
        include(dirname(__FILE__).'/../views/directeur/gestion_salarie.php');
    }

    function onAfficherFiche($id_salarie, $message = null) 
    {
        $salarie = $this->oModel->getOneSalarie($id_salarie);
        $projets = $this->oModel->getProjectSalarie($id_salarie);
        $liste_projets = $this->modelProjet->getAllProject();
		
        include(dirname(__FILE__).'/../views/directeur/modifier_fiche.php');
    }

    function onGestionSalarie() 
    {
		$this->header(5);
        $this->onAfficherPage();
		$this->footer();
    }
    
    function onSalarie() {
        $salarie = $this->oModel->getOneSalarie($_GET['action']);
        $projets = $this->oModel->getProjectSalarie($salarie['id_salarie']);
        $liste_projets = $this->modelProjet->getAllProject();
        $gSalarie = $this->oModel->getAllSalarie();
        
		$this->header(5);
        include(dirname(__FILE__).'/../views/directeur/gestion_salarie.php');
        include(dirname(__FILE__).'/../views/directeur/fiche_salarie.php');
		$this->footer();
    }
    
    function onNouveauSalarie() 
    {
        $code = $_POST['code'];
        $nom = $_POST['nom'];
        $prenom = $_POST['prenom'];
        $mail = $_POST['mail'];
        $login = strtolower($prenom[0].$nom);
        $mdp = hash("sha256",$login,false);
        $droit = 2;
        
		$bool = $this->modelUtilisateur->verifierAdresseMail($mail);
		if($code != null && $nom != null & $prenom != null & $mail != null) {
			if($bool) {
				$this->modelUtilisateur->addUtilisateur($login, $mdp, $mail, $droit);
				/*$this->modelUtilisateur->envoyerMail($login, $mdp, $mail);*/
				$user = $this->modelUtilisateur->getOneUtilisateur($login);
				$this->oModel->addSalarie($code, $nom, $prenom, $mail, $user['id_utilisateur']);
			}
		}
		
        $this->onAfficherPage();
    }
    
    function onSupprimerSalarie() 
    {
        $id = $_POST['id'];
        
        $salarie = $this->oModel->getOneSalarie($id);
        $this->modelUtilisateur->deleteUtilisateur($salarie['id_utilisateur']);
        $this->oModel->deleteSalarie($id);
		
        $this->onAfficherPage();
    }
    
    function onNouveauProjetSalarie() 
    {
        $id_projet = $_POST['projet'];
        $debut = $_POST['debut'];
        $fin = $_POST['fin'];
        $id_salarie = $_POST['id'];
        $debut = implode('-', array_reverse(explode('-', $debut)));
		$fin = implode('-', array_reverse(explode('-', $fin)));
		
		$message = '';
		if($this->oModel->projetExiste($id_projet, $id_salarie)) 
			$message .= '</br> Ce projet existe déjà pour ce salarié.';
		if($debut != '' && $fin != '') {
			if($fin < $debut) 
				$message .= '</br> La date de fin doit être ultérieure à la date de début.';
			else
				$this->oModel->addProjetSalarie($id_salarie, $id_projet, $debut, $fin);
		}
		else {
			if($debut == '' || $fin == '')
				$message .= '</br> Une des deux dates n\'est pas renseignée.';
		}
			
        $this->onAfficherFiche($id_salarie, $message);
    }
    
    function onModifierProjetSalarie() 
    {
        $id_projet = $_POST['id_projet'];
        $id_salarie = $_POST['id_salarie'];
        $debut = $_POST['debut'];
        $fin = $_POST['fin'];
        $debut = implode('-', array_reverse(explode('-', $debut)));
		$fin = implode('-', array_reverse(explode('-', $fin)));
            
		$message = '';
		if($debut != '' && $fin != '') {
			if($fin < $debut) 
				$message .= '</br> La date de fin doit être ultérieure à la date de début.';
			else
				$this->oModel->editProjetSalarie($id_salarie, $id_projet, $debut, $fin);
		}
		else {
			if($debut == '' || $fin == '')
				$message .= '</br> Une des deux dates n\'est pas renseignée.';
		}
		
        $this->onAfficherFiche($id_salarie, $message); 
    }
    
    function onSupprimerProjetSalarie() 
    {
        $id_projet = $_POST['id_projet'];
        $id_salarie = $_POST['id_salarie'];
            
        $this->oModel->deleteProjetSalarie($id_salarie, $id_projet);
		
        $this->onAfficherFiche($id_salarie);
    }
    
    function onModifierFicheSalarie() 
    {
        $id_salarie = $_POST['id_salarie'];
        $code = $_POST['code'];
        $nom = $_POST['nom'];
        $prenom = $_POST['prenom'];
        $mail = $_POST['mail'];
        
		$bool = $this->modelUtilisateur->verifierAdresseMail($mail);
		if($code != null && $nom != null & $prenom != null & $mail != null) {
			if($bool) 
				$this->oModel->editFicheSalarie($id_salarie, $code, $nom, $prenom, $mail);
		}
		
        $salarie = $this->oModel->getOneSalarie($id_salarie);
        $projets = $this->oModel->getProjectSalarie($id_salarie);
        $liste_projets = $this->modelProjet->getAllProject();
        $gSalarie = $this->oModel->getAllSalarie();
        include(dirname(__FILE__).'/../views/directeur/gestion_salarie.php');
        include(dirname(__FILE__).'/../views/directeur/fiche_salarie.php');
    }
    
    function onSupprimerProjetFini() {
        $listeSalarie = $this->oModel->getAllSalarie();
		
		foreach($listeSalarie as $salarie) {
			$listeProjet = $this->oModel->getProjetSalarieFini($salarie['id_salarie']);
			foreach($listeProjet as $projet) 
				$this->oModel->deleteProjetSalarie($salarie['id_salarie'], $projet['id_projet']);
		}
    }
    
    function onEnvoyerMailRappel() {
        $listeSalarie = $this->oModel->getAllSalarie();
		
		foreach($listeSalarie as $salarie) {
			$listeProjet = $this->oModel->getProjectSalarie($salarie['id_salarie']);
			foreach($listeProjet as $projet) {
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= 'To: ' . $salarie['prenom'] . $salarie['nom'] . ' <' . $salarie['mail'] . '>' . "\r\n";
				$headers .= 'From: webmaster@gestion_imputation.com' . "\r\n";
			
				$message = '<p> N\'oubliez pas de remplir votre fichier d\'imputation pour le projet ' . $projet['libelle'] . '. ';
				$message .= 'Pour remplir votre fichier d\'imputation, <a href="www.gestion_imputation.com?action=consulter_imputation"> cliquez ici </a> . </p>';
			
				$this->oModel->mail($salarie['mail'], 'Rappel de l\'imputation', $message, $headers);
			}
		}
    }
}