﻿<?php
require_once('model/Projet.class.php');

class ControllerModele extends Controller {
    private $oModel;
    private $modelProjet;
    private $modelSalarie;
    
    // Constructeur
    function ControllerModele($modelModele, $modelProjet, $modelSalarie) 
	{
        $this->oModel = $modelModele;
        $this->modelProjet = $modelProjet;
        $this->modelSalarie = $modelSalarie;
    }
    
    function onConsultImputation() 
	{
		if($_SESSION['role'] == 'salarie')
			$this->onSaisieImputation();
		else 
			$this->onConsultBibliImputation();
		
    }
    
    function onAfficherPageSaisie($id_saisie, $id_modele) 
	{
		$saisie = $this->oModel->getOneSalarieSaisie($id_saisie);
		$salarie = $this->modelSalarie->getOneSalarieUtilisateur($_SESSION['utilisateur']['id_utilisateur']);
		$listeChamps = $this->oModel->getAllSalarieChamps($saisie['id_salarie_saisie']);
        $modele = $this->oModel->getOneModele($id_modele);
        $listeModeleChamps = $this->oModel->getAllModeleChamps($id_modele);
		
		include(dirname(__FILE__).'/../views/salarie/saisie_modele.php');
    }
    
    function onSaisieImputation() 
	{
		// on récupère l'utilisateur en cours
		$salarie = $this->modelSalarie->getOneSalarieUtilisateur($_SESSION['utilisateur']['id_utilisateur']);
		$listeModele = $this->oModel->getAllModeleSalarie($salarie['id_salarie']);
		// date du mois courant
		setlocale (LC_TIME, 'fr_FR.utf8','fra');
		$annee = strftime("%Y");
		$mois = strftime("%B");
		$date = $mois. ' ' .$annee;
		
		$total = 0;
        $modele = $this->oModel->getOneModele($listeModele[0]['id_modele']);
        $listeModeleChamps = $this->oModel->getAllModeleChamps($modele['id_modele']);
		// on prend le modèle correspondant à ce mois-ci
		$saisie = $this->oModel->getOneSalarieSaisieMois($salarie['id_salarie'], $listeModele[0]['id_modele'], $date);
		// s'il n'existe pas de modèle pour ce mois-ci on le crée
		if($saisie == null) {
			$this->oModel->addSalarieSaisie($salarie['id_salarie'], $listeModele[0]['id_modele'], $date);
			$saisie = $this->oModel->getOneSalarieSaisieMois($salarie['id_salarie'], $listeModele[0]['id_modele'], $date);
			foreach($listeModeleChamps as $modeleChamps) 
				$this->oModel->addSalarieChamps($saisie['id_salarie_saisie'], $modeleChamps['type_activite'], $modeleChamps['decompte'], $modeleChamps['aide']);
		}
		$listeChamps = $this->oModel->getAllSalarieChamps($saisie['id_salarie_saisie']);
		foreach($listeChamps as $champs) {
			if($champs['decompte'] == 1) 
				$total += $champs['nbr_jours'];
		}
		
		// on affiche le résultat
		$this->header();
		include(dirname(__FILE__).'/../views/salarie/saisie_imputation.php');
		$this->footer();
    }
    
    function onChangerMoisSaisie() 
	{
		// on récupère les données envoyées en ajax
		$id_modele = $_POST['id_modele'];
		$date = $_POST['mois'];
		
		$total = 0;
		$salarie = $this->modelSalarie->getOneSalarieUtilisateur($_SESSION['utilisateur']['id_utilisateur']);
        $listeModeleChamps = $this->oModel->getAllModeleChamps($id_modele);
        $modele = $this->oModel->getOneModele($id_modele);
		// on prend le modèle correspondant à ce mois-ci
		$saisie = $this->oModel->getOneSalarieSaisieMois($salarie['id_salarie'], $id_modele, $date);
		// s'il n'existe pas de modèle pour ce mois-ci on le crée
		if($saisie == null) {
			$this->oModel->addSalarieSaisie($salarie['id_salarie'], $id_modele, $date);
			$saisie = $this->oModel->getOneSalarieSaisieMois($salarie['id_salarie'], $id_modele, $date);
			foreach($listeModeleChamps as $modeleChamps) 
				$this->oModel->addSalarieChamps($saisie['id_salarie_saisie'], $modeleChamps['type_activite'], $modeleChamps['decompte'], $modeleChamps['aide']);
		}
		$listeChamps = $this->oModel->getAllSalarieChamps($saisie['id_salarie_saisie']);
		foreach($listeChamps as $champs) {
			$champs = $this->oModel->getOneSalarieChamps($champs['id_salarie_champs']);
			if($champs['decompte'] == 1) 
				$total += $champs['nbr_jours'];
		}
		
		include(dirname(__FILE__).'/../views/salarie/changer_modele_saisie.php');
		
    }
    
    function onChangerModeleSaisie() 
	{
		$id_modele = $_POST['id_modele'];
		// date du mois courant
		setlocale (LC_TIME, 'fr_FR.utf8','fra');
		$annee = strftime("%Y");
		$mois = strftime("%B");
		$date = $mois. ' ' .$annee;
		
		$total = 0;
		$salarie = $this->modelSalarie->getOneSalarieUtilisateur($_SESSION['utilisateur']['id_utilisateur']);
        $listeModeleChamps = $this->oModel->getAllModeleChamps($id_modele);
        $modele = $this->oModel->getOneModele($id_modele);
		// on prend le modèle correspondant à ce mois-ci
		$saisie = $this->oModel->getOneSalarieSaisieMois($salarie['id_salarie'], $id_modele, $date);
		// s'il n'existe pas de modèle pour ce mois-ci on le crée
		if($saisie == null) {
			$this->oModel->addSalarieSaisie($salarie['id_salarie'], $id_modele, $date);
			$saisie = $this->oModel->getOneSalarieSaisieMois($salarie['id_salarie'], $id_modele, $date);
			foreach($listeModeleChamps as $modeleChamps) 
				$this->oModel->addSalarieChamps($saisie['id_salarie_saisie'], $modeleChamps['type_activite'], $modeleChamps['decompte'], $modeleChamps['aide']);
		}
		$listeChamps = $this->oModel->getAllSalarieChamps($saisie['id_salarie_saisie']);
		foreach($listeChamps as $champs) {
			$champs = $this->oModel->getOneSalarieChamps($champs['id_salarie_champs']);
			if($champs['decompte'] == 1) 
				$total += $champs['nbr_jours'];
		}
		
		include(dirname(__FILE__).'/../views/salarie/changer_modele_saisie.php');
    }
	
    function onValiderModeleSaisie() 
	{
		$nbr_jours = $_POST['nbr_jours'];
		$commentaire = $_POST['commentaire'];
		$id_salarie_champs = $_POST['id_salarie_champs'];
		$id_modele = $_POST['modele'];
		
		$champs = $this->oModel->getOneSalarieChamps($id_salarie_champs);
		$saisie = $this->oModel->getOneSalarieSaisie($champs['id_salarie_saisie']);
		
		if($nbr_jours > 0) {
			if($champs['decompte'] == 1)
				$total = $saisie['total'] + $nbr_jours;
		}
		
		$this->oModel->updateSalarieChamps($nbr_jours, $commentaire, $id_salarie_champs);
		$this->oModel->updateSalarieSaisie($total, $saisie['id_salarie_saisie']);
		
		$this->onAfficherPageSaisie($saisie['id_salarie_saisie'], $id_modele);
    }
    
    function onSupprimerModeleSaisie() 
	{
		$id_salarie_champs = $_POST['id_salarie_champs'];
		$id_saisie = $_POST['saisie'];
		$id_modele = $_POST['modele'];
		
		$this->oModel->deleteSalarieChamps($id_salarie_champs);
		
		$this->onAfficherPageSaisie($id_saisie, $id_modele);
    }
    
    function onConsultBibliImputation() 
	{
		$listeSalarieSaisie = $this->oModel->getAllSalarieSaisie();
		
		$this->header();
		include(dirname(__FILE__).'/../views/directeur/bibli_imputation.php');
		$this->footer();
    }
    
    function onAfficherPage($id) 
	{
        $modele = $this->oModel->getOneModele($id);
		$listeModele = $this->oModel->getAllModele();
        $listeModeleChamps = $this->oModel->getAllModeleChamps($id);
        $listeChamps = $this->oModel->getAllChamps();
		$listeProjets = $this->modelProjet->getAllProject();
		
		include(dirname(__FILE__).'/../views/directeur/modele_imputation.php');
    }
    
    function onModeleImputation($id) 
	{
		$this->header(2);
		$this->onAfficherPage($id);
		$this->footer();
    }
    
    function onChangerModeleImputation() 
	{
		$id = $_POST['id_modele'];
		$this->onAfficherPage($id);
    }
    
    function onNouveauModeleImputation() 
	{
		$nom = $_POST['nom'];
        $modele = $this->oModel->getByNom($nom);
		if(isset($modele))
			echo 'Ce modèle existe déjà.';
		else {
			$this->oModel->addModele($nom);
			$modele = $this->oModel->getByNom($nom);
		}
        $listeChamps = $this->oModel->getAllChamps();
		$listeModele = $this->oModel->getAllModele();
		$listeProjets = $this->modelProjet->getAllProject();
		
		include(dirname(__FILE__).'/../views/directeur/modele_imputation.php');
    }
    
    function onCopierModeleImputation() 
	{
		$id_modele = $_POST['modele'];
		$nom = $_POST['nom'];
        $modele = $this->oModel->getByNom($nom);
		if(isset($modele))
			echo 'Ce modèle existe déjà.';
		else {
			$this->oModel->addModele($nom);
			$modele = $this->oModel->getByNom($nom);
		}
        $listeModeleChamps = $this->oModel->getAllModeleChamps($id_modele);
        $listeChamps = $this->oModel->getAllChamps();
		$listeModele = $this->oModel->getAllModele();
		$listeProjets = $this->modelProjet->getAllProject();
		
		// on copie tous les champs du modèle dans le nouveau
		if(isset($listeModeleChamps)) {
			foreach($listeModeleChamps as $modeleChamps) 
				$this->oModel->addModeleChamps($modeleChamps['type_activite'], $modeleChamps['id_champs'], $modeleChamps['decompte'], $modeleChamps['aide'], $modele['id_modele']);
		}
		
		include(dirname(__FILE__).'/../views/directeur/modele_imputation.php');
    }
    
    function onLierModeleImputation() 
	{
		$id_projet = $_POST['projet'];
		$id_modele = $_POST['modele'];
        
        $modele = $this->modelProjet->getOneProjet($id_projet);
		$deja = $this->oModel->editProjetDejaModele($id_projet);
		if(isset($deja)) 
			echo 'Le projet ' .$modele['libelle_projet']. ' est déjà lié à un modèle.';
		else {
			$this->oModel->editProjetModele($id_projet, $id_modele);
			echo 'Le modèle a été lié au projet ' .$modele['libelle_projet']. '.';
		}
    }
    
    function onAfficherModele($id_modele) 
    {
        $modele = $this->oModel->getOneModele($id_modele);
        $listeModeleChamps = $this->oModel->getAllModeleChamps($id_modele);
        $listeChamps = $this->oModel->getAllChamps();
		
		include(dirname(__FILE__).'/../views/directeur/modele.php');
    }
    
    function onNouveauModeleChamps() 
    {
        $id_modele = $_POST['id_modele'];
        $type_activite = $_POST['type_activite'];
        $type_champs = $_POST['type_champs'];
        $decompte = $_POST['decompte'];
        $aide = $_POST['aide'];
        if($decompte == false)
            $decompte = 0;
        else
            $decompte = 1;

        $modele = $this->oModel->addModeleChamps($type_activite, $type_champs, $decompte, $aide, $id_modele);
        
		$this->onAfficherModele($id_modele);
    }
    
    function onModifierModeleChamps() 
    {
        $id_modele_champs = $_POST['id_modele_champs'];
        $type_activite = $_POST['type_activite'];
        $type_champs = $_POST['type_champs'];
        $decompte = $_POST['decompte'];
        $aide = $_POST['aide'];
        if($decompte == false)
            $decompte = 0;
        else
            $decompte = 1;
            
        $this->oModel->editModeleChamps($type_activite, $type_champs, $decompte, $aide, $id_modele_champs);
        
		$this->onAfficherModele($_POST['id_modele']);
    }
    
    function onSupprimerModeleChamps() 
    {
        $id_modele_champs = $_POST['id_modele_champs'];
            
        $this->oModel->deleteModeleChamps($id_modele_champs);
        
		$this->onAfficherModele($_POST['id_modele']);
    }
}