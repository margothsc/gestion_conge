<?php
	require_once '../PHPExcel/Classes/PHPExcel.php';
	require_once '../PHPExcel/Classes/PHPExcel/Writer/Excel2007.php';
	require_once '../model/Salarie.class.php';
	require_once '../model/Modele.class.php';
                
	$modelSalarie = new Salarie();
	$salarie = $modelSalarie->getOneSalarie($_GET['id_salarie']);
	$modelModele = new Modele();
	$salarie_saisie = $modelModele->getOneSalarieSaisie($_GET['id_salarie_saisie']);
	$liste_champs = $modelModele->getAllSalarieChamps($salarie_saisie['id_salarie_saisie']);
				
	$workbook = new PHPExcel;

	$workbook->getProperties()->setCreator("Gestion_imputation")
		->setLastModifiedBy("Gestion_imputation")
		->setTitle("Imputation")
		->setSubject("Imputation")
		->setDescription("Test document for Office 2005 XLSX, generated using PHP classes.")
		->setKeywords("office 2005 openxml php");
		
	$workbook->getActiveSheet()->getColumnDimension('A')->setWidth(25);
	$workbook->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$workbook->getActiveSheet()->getColumnDimension('C')->setWidth(50);
	$workbook->getActiveSheet()->getColumnDimension('D')->setWidth(50);
	
	$sheet = $workbook->getActiveSheet();
	$sheet->setCellValue('A1', 'Nom')
		->setCellValue('B1', $salarie['nom'])
		->setCellValue('A2', 'Prenom')
		->setCellValue('B2', $salarie['prenom'])
		->setCellValue('A3', 'Mois')
		->setCellValue('B3', $salarie_saisie['mois'])
		->setCellValue('A6', 'Relevé d\'activité')
		->setCellValue('A7', 'Type d\'activité')
		->setCellValue('B7', 'Nombre de jours')
		->setCellValue('C7', 'Commentaire')
		->setCellValue('D7', 'Aide');
		
	// afichage des champs
	$debut = 8;
	$total = 0;
	if(isset($liste_champs)) {
		foreach($liste_champs as $salarie_champs) {
			$sheet->setCellValue('A'.$debut, $salarie_champs['type_activite'])
				->setCellValue('B'.$debut, $salarie_champs['nbr_jours'])
				->setCellValue('C'.$debut, $salarie_champs['commentaire'])
				->setCellValue('D'.$debut, $salarie_champs['aide']);
				if($salarie_champs['decompte'] == 1) 
					$total += $salarie_champs['nbr_jours'];
			$debut += 1;
		}
	}
	$sheet->setCellValue('A'.$debut, 'Total')
		->setCellValue('B'.$debut, $total);

	// bordures
	$styleArray = array(
	    'borders' => array(
			'outline' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			),
		),
	);
	$sheet->getStyle('A6:D6')->applyFromArray($styleArray);
	for($i = 7 ; $i < $debut ; $i++) {
		$sheet->getStyle('A'.$i)->applyFromArray($styleArray);
		$sheet->getStyle('B'.$i)->applyFromArray($styleArray);
		$sheet->getStyle('C'.$i)->applyFromArray($styleArray);
		$sheet->getStyle('D'.$i)->applyFromArray($styleArray);
	}
	$sheet->getStyle('A'.$debut)->applyFromArray($styleArray);
	$sheet->getStyle('B'.$debut)->applyFromArray($styleArray);
		
	$writer = new PHPExcel_Writer_Excel2007($workbook);

	header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition:inline;filename=Fichier.xlsx ');
	$writer->save('php://output');