<?php
require_once('model/Projet.class.php');

class ControllerProjet extends Controller {
    private $oModel;
    
    // Constructeur
    function ControllerProjet($modelProjet) 
	{
        $this->oModel = $modelProjet;
    }

    function onAfficherPage() 
	{
		$projets = $this->oModel->getAllProject();
		include(dirname(__FILE__).'/../views/directeur/gestion_projet.php');
    }

    function onGestionProjet() 
	{
		$this->header(6);
		$this->onAfficherPage();
		$this->footer();
    }
    
    function onNouveauProjet() 
	{
		$libelle = $_POST['nom'];
		
		if($libelle != null)
			$this->oModel->addProject($libelle);
			
		$this->onAfficherPage();
    }
    
    function onModifierProjet() 
	{
		$libelle = $_POST['nom'];
		$id = $_POST['id'];
	
		if($libelle != null)
			$this->oModel->editProject($id, $libelle);
			
		$this->onAfficherPage();
    }
    
    function onSupprimerProjet() 
	{
		$id = $_POST['id'];
		
		$this->oModel->deleteProject($id);
		
		$this->onAfficherPage();
    }
}