<?php
class ControllerView {

    // Appelée lorsque l'utilisateur génère un évènement non supporté
    function onMethodNotImplemented() 
	{
        $this->oView->envoyerErreurServeur(501);
    }
	
	function onStart($strErrorMsg = '') {
		if(!isset($_SESSION['seed']))
			$_SESSION['seed'] = rand(64,1024);
		
		include (dirname(__FILE__).'/../views/template.php');
		include(dirname(__FILE__).'/../views/connexion.php');
		include (dirname(__FILE__).'/../views/pied_page.php');
	}
    
    function onLog() {
		include (dirname(__FILE__).'/../views/template.php');
		$this->onConsultImputation();
		include (dirname(__FILE__).'/../views/pied_page.php');
    }
}