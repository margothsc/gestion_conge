<?php
require_once('model/Utilisateur.class.php');

class ControllerUtilisateur extends Controller {
    private $oModel;
    
    // Constructeur
    function ControllerUtilisateur($modelUtilisateur) 
	{
        $this->oModel = $modelUtilisateur;
    }

    function onAfficherPage() 
    {
        $roles = $this->oModel->getAllDroit();
        $utilisateurs = $this->oModel->getAllUtilisateur();
        include(dirname(__FILE__).'/../views/directeur/gestion_utilisateur.php');
    }

    function onGestionUtilisateur() 
    {
		$this->header(7);
        $this->onAfficherPage();
		$this->footer();
    }
    
    function onNouvelUtilisateur() 
    {
        $login = $_POST['login'];
        $mdp = $_POST['mdp'];
        $mail = $_POST['mail'];
        $droit = $_POST['droit']; 
        
		$bool = $this->oModel->verifierAdresseMail($mail);
		if($login != null && $mdp != null) {
			if($bool) {
				$this->oModel->addUtilisateur($login, $mdp, $mail, $droit);
				$this->oModel->envoyerMail($login, $mdp, $mail);
			}
		}
        $this->onAfficherPage();
    }
    
    function onModifierUtilisateur() 
    {
        $login = $_POST['login'];
        $id = $_POST['id'];
        $mail = $_POST['mail'];
        $role = $_POST['droit'];
        
        if($role == 'salarie')
            $role = 2;
        else    
            $role = 1;
		
		$bool = $this->oModel->verifierAdresseMail($mail);
		if($login != null) {
			if($bool)
				$this->oModel->updateUtilisateur($id, $login, $mail, $role);
		}
        $this->onAfficherPage();
    }
    
    function onSupprimerUtilisateur() 
    {
        $id = $_POST['id'];
    
        $this->oModel->deleteUtilisateur($id);
		
        $this->onAfficherPage();
    }
    
    function onModifierMdpUtilisateur() 
    {
        $login = $_SESSION['login'];
        $mdp = $_POST['mdp'];
        $passCrypt = $_POST['passCrypt'];
    
        if($this->oModel->isMdpUtilisateur($login, $mdp)) {
			$this->oModel->setMdpUtilisateur($login, $passCrypt);
			echo 'Le mot de passe a été modifié.';
		}
		else
			echo 'L\'ancien mot de passe est différent de celui spécifié.';
    }
}