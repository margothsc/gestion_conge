<?php
	require_once '../PHPExcel/Classes/PHPExcel.php';
	require_once '../PHPExcel/Classes/PHPExcel/Writer/Excel2007.php';
	require_once '../model/Salarie.class.php';
	require_once '../model/Modele.class.php';
                
	$modelSalarie = new Salarie();
	$modelModele = new Modele();
	
	// nombre de feuilles
	$j = 0;
	$liste_salarie = $modelModele->getSalarieWithSaisie();
			
	$workbook = new PHPExcel;

	$workbook->getProperties()->setCreator("Gestion_imputation")
		->setLastModifiedBy("Gestion_imputation")
		->setTitle("Imputation")
		->setSubject("Imputation")
		->setDescription("Test document for Office 2005 XLSX, generated using PHP classes.")
		->setKeywords("office 2005 openxml php");
	$sheet = $workbook->getActiveSheet();
		
	foreach($liste_salarie as $id_salarie) {
		$salarie = $modelSalarie->getOneSalarie($id_salarie['id_salarie']);
		$liste_saisie = $modelModele->getAllSaisieBySalarie($id_salarie['id_salarie']);
		
		if($j > 0) {
			$sheet = $workbook->createSheet();
		}
		$sheet->setTitle($salarie['nom']);
		$j++;
		// nombre de modèles
		$debut_modele = 1;
		foreach($liste_saisie as $salarie_saisie) {
			$liste_champs = $modelModele->getAllSalarieChamps($salarie_saisie['id_salarie_saisie']);
			
			$sheet->getColumnDimension('A')->setWidth(25);
			$sheet->getColumnDimension('B')->setAutoSize(true);
			$sheet->getColumnDimension('C')->setWidth(50);
			$sheet->getColumnDimension('D')->setWidth(50);
					
			$sheet->setCellValue('A'.$debut_modele, 'Nom')
				->setCellValue('B'.$debut_modele, $salarie['nom']) ;
			$debut_modele++ ;
			$sheet->setCellValue('A'.$debut_modele, 'Prenom')
				->setCellValue('B'.$debut_modele, $salarie['prenom']) ;
			$debut_modele++ ;
			$sheet->setCellValue('A'.$debut_modele, 'Mois')
				->setCellValue('B'.$debut_modele, $salarie_saisie['mois']) ;
			$debut_modele++ ;
			$sheet->setCellValue('A'.$debut_modele, 'Relevé d\'activité') ;
			$debut_modele += 3 ;
			$sheet->setCellValue('A'.$debut_modele, 'Type d\'activité')
				->setCellValue('B'.$debut_modele, 'Nombre de jours')
				->setCellValue('C'.$debut_modele, 'Commentaire')
				->setCellValue('D'.$debut_modele, 'Aide');
				
			$debut_modele++ ;
			$debut_champs = $debut_modele;
			$total = 0;
			if(isset($liste_champs)) {
				foreach($liste_champs as $salarie_champs) {
					$sheet->setCellValue('A'.$debut_modele, $salarie_champs['type_activite'])
						->setCellValue('B'.$debut_modele, $salarie_champs['nbr_jours'])
						->setCellValue('C'.$debut_modele, $salarie_champs['commentaire'])
						->setCellValue('D'.$debut_modele, $salarie_champs['aide']) ;
						if($salarie_champs['decompte'] == 1) 
							$total += $salarie_champs['nbr_jours'] ;
					$debut_modele++ ;
				}
			}
			$sheet->setCellValue('A'.$debut_modele, 'Total')
				->setCellValue('B'.$debut_modele, $total) ;

			// bordures
			$styleArray = array(
				'borders' => array(
					'outline' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					),
				),
			);
			$sheet->getStyle('A6:D6')->applyFromArray($styleArray);
			for($i = $debut_champs ; $i < $debut_modele ; $i++) {
				$sheet->getStyle('A'.$i)->applyFromArray($styleArray);
				$sheet->getStyle('B'.$i)->applyFromArray($styleArray);
				$sheet->getStyle('C'.$i)->applyFromArray($styleArray);
				$sheet->getStyle('D'.$i)->applyFromArray($styleArray);
			}
			$sheet->getStyle('A'.$debut_modele)->applyFromArray($styleArray);
			$sheet->getStyle('B'.$debut_modele)->applyFromArray($styleArray);
			
			
			$debut_modele += 5 ;
		}
	}
	
	$writer = new PHPExcel_Writer_Excel2007($workbook);

	header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition:inline;filename=Fichier.xlsx ');
	$writer->save('php://output');