<?php
require_once('model/Conge.class.php');
require_once ('PHPExcel/Classes/PHPExcel/IOFactory.php');

class ControllerConge extends Controller {
    private $oModel;
    private $modelProjet;
    private $dossier;
    
    // Constructeur
    function ControllerConge($modelConge, $modelProjet) 
	{
        $this->oModel = $modelConge;
        $this->modelProjet = $modelProjet;
		$this->dossier = dirname(__FILE__) . '/../upload/';
    }

    function onConsultConge() 
	{
		$conge = $this->oModel->getRecentConge();
		return $this->onAfficherConge($conge);
    }

    function onChangerConge() 
	{
        $semestre = $_POST['semestre'];
        $annee = $_POST['annee'];
        $projet = $_POST['projet']; 
		
		$conge = $this->oModel->getOneConge($semestre, $annee, $projet);
		if(isset($conge))
			return $this->onAfficherConge($conge);
		else
			return $this->onCongeInexistant();
    }

    function onAfficherConge($conge) 
	{
		$date = date('Y');
		$listeProjets = $this->modelProjet->getAllProject();
		
		$this->header(3);
		echo '<p> Congés concernant le projet ' .$conge['libelle_projet']. '</p>';
		echo '<p> Pour le semestre ' .$conge['semestre']. ' de l\'année ' .$conge['annee']. '</p>';
		echo '</br>';
		// Chargement du fichier Excel
		$objPHPExcel = PHPExcel_IOFactory::load($this->dossier . $conge['nom']);
		$sheet = $objPHPExcel->getSheetByName("Fichier Ausy");
		 
		// affichage du fichier
		echo '<table style="margin-bottom:50px;">';
		 
		foreach($sheet->getRowIterator() as $row) {
		   echo '<tr>';
		   foreach ($row->getCellIterator() as $cell) {
			  echo '<td style="border:1px solid black;padding:5px">';
			  print_r($cell->getFormattedValue());
			  echo '</td>';
		   }
		   echo '</tr>';
		}
		
		echo '</table>';
		echo '<a href="' .$this->oModel->getURLofPath($this->dossier). '/' . $conge['nom']. '"> <button type="button"> Exporter </button> </a>';
		include(dirname(__FILE__).'/../views/directeur/consulter_conge.php');
		$this->footer();
    }
    
    function onNouveauFichierConge() 
    {
		$listeProjets = $this->modelProjet->getAllProject();
		$date = date('Y');
		$this->header(4);
		include(dirname(__FILE__).'/../views/directeur/ajouter_fichier_conge.php');
		$this->footer();
    }
    
    function onImporterFichierConge() 
    {
        $semestre = $_POST['semestre'];
        $annee = $_POST['annee'];
        $projet = $_POST['projet']; 
        
		// récupère la partie de la chaine à partir du dernier . pour connaître l'extension.
		$extension = strrchr($_FILES['conge']['name'], '.');
		$extensions = array('.xls', '.xlsx');
		if(in_array($extension, $extensions)) {
			$taille_maxi = 100000;
			$taille = filesize($_FILES['conge']['tmp_name']);
			if($taille>$taille_maxi)
				 $erreur = 'Le fichier est trop gros';
			
			$fichier = 'conge' .time(). '.xls';
			if(move_uploaded_file($_FILES['conge']['tmp_name'], $this->dossier . $fichier)) {
				$conge = $this->oModel->getOneConge($semestre, $annee, $projet);
				if(isset($conge))
					$this->oModel->updateConge($fichier, $conge['id_conge']);
				else	
					$this->oModel->addConge($semestre, $annee, $projet, $fichier);
				return $this->onConsultConge();
			}
			else
				return $this->onNouveauFichierConge();
		}
		else
			return $this->onNouveauFichierConge();
    }

    function onCongeInexistant() 
	{
		$this->header(3);
		include(dirname(__FILE__).'/../views/erreur/conge_inexistant.php');
		$this->footer();
    }
}